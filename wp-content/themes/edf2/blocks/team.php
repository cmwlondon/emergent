<?php 

/*
ACF 'Our team'
team_title
team_introduction
team_members[
  team_member[
    custom post type 'team_members'
  ],
  ...
]

custom post type 'team_members'
ACF fields:
  team_name
  team_role
  team_email
  team_bio
  team_linkedin
  team_photo (image URL)


*/
$team_members = get_field('team_members');
$teamTitle = word_wrapper(get_field('team_title'));
$teamIntroduction = ( get_field('team_introduction') !== '' ) ? word_wrapper(get_field('team_introduction')) : '';

?>

    <section class="module team width90percent">
      <header>
        <h2><?php _e( $teamTitle ); ?></h2>
        <?php if ( $teamIntroduction !== '' ) : ?><p><?php _e( $teamIntroduction ); ?></p><?php endif; ?>
      </header>

      <div class="row">
        
<?php
if ($team_members) :
  foreach($team_members AS $team_index => $item) :
    $team_member_id = $item['team_member']->ID;
    $name = get_field('team_name', $team_member_id);
    $role = word_wrapper( get_field('team_role', $team_member_id) );
    $photo = get_field('team_photo', $team_member_id);
    $email = get_field('team_email', $team_member_id);
    $linkedin = get_field('team_linkedin', $team_member_id);
    $bio = get_field('team_bio', $team_member_id);
?>
        <div class="col-sm-4" id="team<?php echo $team_member_id; ?>">
          <div class="copybox">

            <?php if ( $photo ) : ?>
            <figure><img alt="<?php _e($name); ?>" src="<?php echo $photo; ?>"></figure>
            <?php endif; ?>

            <div class="name">
              <h3><?php _e($name); ?></h3>
              <h4><?php _e($role); ?></h4>

              <?php if ( $email ) : ?>
              <p><?php echo $email; ?></p>
              <?php endif; ?>

              <?php if ( $linkedin ) : ?>
              <a href="<?php echo $linkedin; ?>" class="linkedin" target="_blank" title="linkedin: <?php _e($name); ?>"><img alt="linkedin: <?php _e($name); ?>" src="<?php echo get_template_directory_uri(); ?>/svg/icons/linkedin-black.svg"></a>
              <?php endif; ?>

              <?php if ( $bio ) : ?>
              <div class="slideA">
                <div class="slideB">
                  <p><?php _e($bio); ?></p>
                </div>
              </div>
              <?php endif; ?>

            </div>
          </div>
        </div>
<?php
  endforeach;
endif;
?>

      </div>


      </div>
    </section>
