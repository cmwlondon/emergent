<?php
$noLogo = !( !is_null($args['showlogo']) && $args['showlogo'] === 'no' );
?>
<section class="module tail <?php echo $args['background_color_class'];?> <?php if ( !$noLogo ) : echo 'noLogo'; endif; ?>">
    <div class="background <?php if ( $args['image'] ) : ?>imageFade<?php endif; ?>">
        <?php
        // image id or false
        if ( $args['image'] ) :
        ?>
        <!-- <img alt="" class="bg" src=""> -->
        <?php echo wp_get_attachment_image( $args['image'], 'full', false, ['class' => 'bg'] ); ?>
        <div class="top">

        <?php
        if ( $args['quote1'] !== '' || $args['quote2'] !== '' ) :
        ?>
        <div class="caption">            
            <?php
            if ( $args['quote1'] !== '' ) :
            ?>
                <p class="quote">&ldquo;<?php echo $args['quote1']; ?>&rdquo;</p>
            <?php 
            endif
            ?>
            <?php
            if ( $args['quote2'] !== '' ) :
            ?>
                <p><?php echo $args['quote2']; ?></p>
            <?php 
            endif
            ?>
        </div>

        <?php 
        endif
        ?>
        </div>
        <?php 
        endif
        ?>

        <div class="bottom">
          <?php if ( $noLogo ) : ?><img class="green-logo" src="<?php echo get_template_directory_uri(); ?>/svg/emergent/green.svg" /> <?php endif; ?>
          <img class="curve" src="<?php echo get_template_directory_uri(); ?>/svg/wave.svg" />
          <div class="message">
            <h2>Get in touch</h2>
            <p>If you have any questions about our high-integrity carbon credits, click <a href="<?php echo site_url(); ?>/get-in-touch">here</a> to make an enquiry.</p>
          </div>
        </div>
    </div>
</section>