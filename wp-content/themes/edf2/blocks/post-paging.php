<?php
  if ( $args['maxpages'] > 1 ) :
?>

<div class="row paging">
  <div class="col">
    <p>page: <?php echo $args['paged']; ?> of <?php echo $args['maxpages']; ?></p>

<?php
    // next_posts_link( __( 'Older Entries', 'textdomain' ), $blogposts->max_num_pages );
?>
    <ul class="paging">
<?php
  $page = 1;
  while ( $page < ($args['maxpages'] + 1) ) :
    if ( $page !== $args['paged'] ) :
?>
      <li><a href="/news-views/page/<?php echo $page; ?>/"><?php echo $page; ?></a></li>
<?php
    else :
?>
      <li class="current"><?php echo $page;?> <span>(current page)</span></li>
<?php
    endif;

    $page++;
  endwhile;
?>
    </ul>
<?php
// previous_posts_link( __( 'Newer Entries', 'textdomain' ) );
?>
  </div>
</div>
<?php
endif;
?>