    <section class="module fullwWidthVideo">
      <div class="row">
        <div class="videoPlayer" data-type="youtube" data-source="ibJ4qNzz8gc" id="vp05">
          <div class="poster"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/misc/how-we-operate.png"></div>
          <div class="video youtube">
            <iframe id="yt02" class="yt_player_iframe" src="https://www.youtube.com/embed/ibJ4qNzz8gc?enablejsapi=1&version=3&playerapiid=ytplayer" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
          </div>
          <a href="" class="play"></a>
        </div>
      </div>  
    </section>
