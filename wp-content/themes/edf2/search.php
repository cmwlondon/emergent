<?php
// search results
?>

<?php get_header( null, [
	'page_banner_class' => '',
	'page_banner_title' => 'Search Results',
	'page_banner_copy' => $wp_query->found_posts.' '.translate( 'Search Results Found For', 'locale' ).' : "'.get_search_query().'"',
	'page_banner_image' => get_template_directory_uri().'/images/home/header.jpg'
] ); ?>

<?php
// https://developer.wordpress.org/reference/functions/get_search_query/
// https://stackoverflow.com/questions/14802498/how-to-display-wordpress-search-results/14802739
// The Query
$s = get_search_query();
$args = array(
	's' => $s
);
$the_query = new WP_Query( $args );
?>
    <section class="module searchResults">
<?php
if ( $the_query->have_posts() ) {
	_e("<header>");
	_e("<h2>Search Results for: \"".get_query_var('s')."\"</h2>");
	_e("</header>");
	_e("<div class=\"row\">");
	_e("<div class=\"col\">");

	_e("<ul>");
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
			<li>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</li>
		<?php
	}
	_e("</ul>");
	_e("</div>");
	_e("</div>");
} else {
?>
<header>
<h2>Nothing Found</h2>
</header>
<?php
}
?>
    </section>

<?php
get_template_part( 'blocks/tail','',[
  'background_color_class' => '',
  'image' => '',
  'quote1' => '',
  'quote2' => ''
] );

get_footer();
?>
