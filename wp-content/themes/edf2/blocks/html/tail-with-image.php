    <section class="module tail">
      <div class="background">
        
        <img alt="" class="bgImage" src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch/mother.jpg">
        
        <div class="wave">
          <div class="caption">
            <p class="quote">&ldquo;Emergent only offers credits that protect forests at massive scale.&rdquo;</p>
            <p>Eron Bloomgarden | Executive Director</p>
          </div>
          <img class="ablogo" src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch/logo-x.png">
          <img class="curve" src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch/curve.png">
          <div class="message">
            <h2>Get in touch</h2>
            <p>If you have any questions about our high-integrity carbon credits, please don’t hesitate to email us at <a href="mailto:info@emergentclimate.com">info@emergentclimate.com</a>.</p>
          </div>
        </div>

      </div>
    </section>
