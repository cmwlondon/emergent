    <section class="module team width90percent">
      <header>
        <h2>One team. One vision.</h2>
        <p>The Emergent team is an experienced, multifaceted group of individuals, who share a vision for more national-scale forest protection for the planet — and everyone on&nbsp;it.</p>
      </header>

      <div class="row">
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="copybox">
            <h3>Forename Surname</h3>
            <h4>Role</h4>
            <p>Duis sit amet consectetur nunc. Sed ornare mattis volutpat. Duis semper consequat orci, a blandit elit euismod&nbsp;vitae.</p>
          </div>
        </div>
      </div>
    </section>
