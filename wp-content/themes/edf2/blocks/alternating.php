<?php
/*
alternating
title
copy
rows
  row
    backround_color 'lg' or 'dg' *
    layout 'vl' or 'vr' *
    type 'image','video' *
    image (doubles as video placeholder) *
    video[
      format 'youtube','vimeo','inline'
      source
      caption
    ]
    heading *
    copy *
    has_a_link 'no','yes'
    link[
    ]
      text
      url
      external_link 'external'.'internal'
    ],
*/
?>
    <section class="module alternatingPairs">
      <?php if (get_field('title') !== '' || get_field('copy') !== '') : ?>
      <header>
        <h2><?php echo get_field('title'); ?></h2>
        <?php if (get_field('copy') !== '') : ?><p><?php echo get_field('copy'); ?></p><?php endif;?>
      </header>
      <?php endif; ?>

      <?php
        if( have_rows('rows') ):

          foreach( get_field('rows') AS $index => $row) :
            $item = $row['row'];
            $rowType = $item['type'];
            $rowImage = $item['image'];
            $rowImageURL = wp_get_attachment_image_url( $rowImage, 'full' );
            
      ?>
      <div class="row <?php echo $item['background_color'];?> <?php echo $item['layout'];?>" id="row<?php echo $index; ?>">

        <div class="col-lg-7 vp">
          <?php
            switch($rowType) {
              case 'image' : {
          ?>
          <div class="staticImage"><figure><img alt="" src="<?php echo $rowImageURL; ?>"></figure></div>
          <?php
              } break;
              case 'video' : {
                $video = $item['video'];
          ?>
          <div class="videoPlayer" data-type="<?php echo $video['format']; ?>" data-source="<?php echo $video['source']; ?>" id="vp<?php echo $index; ?>">
            <div class="poster"><img alt="" src="<?php echo $rowImageURL; ?>"></div>
            <div class="video <?php echo $video['format']; ?>">

          <?php 
              switch($video['format']) {
                case 'youtube' : {
              ?>
              <iframe id="yt<?php echo $index; ?>" class="yt_player_iframe" src="https://www.youtube.com/embed/<?php echo $video['source']; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
              <?php
                } break;
                case 'vimeo' : {
              ?>
              <iframe src="https://player.vimeo.com/video/78535150?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              <?php
                } break;
                case 'ted' : {
              ?>
              <?php
                } break;
                case 'inline' : {
              ?>
              <video preload="metadata" controls="controls" playsinline="" noloop="">
                <source type="video/mp4" src="public/video/sample.mp4">
              </video>
              <?php
                } break;
              }
          ?>    
            </div>
            <a href="" class="play"></a>
          </div>          
          <?php
              } break;
            };
          ?>
        </div>

        <div class="col-lg-5 caption align-items-center">
          <div class="captionInner">
            <div class="captionEvenInner">
              <h3><?php echo $item['heading']; ?></h3>
              <div><p><?php echo $item['copy'] ?></p></div>
              <?php
              if ( $item['has_a_link'] === 'yes' ) :
                $link = $item['link'];
              ?>
                  <a href="<?php echo $link['url']; ?>" <?php if ( $link['external_link'] === 'external') {
                    ?>target="_blank"<?php
                  } ?>><?php echo $link['text']; ?></a>
              <?php endif; ?>
            </div>
          </div>
        </div>

      </div>
      <?php
          endforeach;
        endif;
      ?>
    </section>



