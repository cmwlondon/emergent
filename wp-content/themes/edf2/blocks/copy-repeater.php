<?php
/*
copy-repeater
v2

copy_blocks[
	copy_block[
		copy_block_type 'text', 'video'

		text_block[
			text_css
			text_has_background
			text_background URL
			text_heading
			text_column_layout (number of columns)
			text_columns[
				text_column[
					text_column_css
					text_column_heading
					text_column_copy
				]
				....
			]
		]

		video_block[
			video_format
			video_source
			video_poster
			video_caption
			video_start
		]
		
	]
	...
]
*/

$videoIdentifier = 0;

$blocks = get_field('copy_blocks');
if ( count($blocks) > 0 ) :
	foreach( $blocks AS $blokcIndex => $blockItem) :
	$item = $blockItem['copy_block'];
	$type = $item['copy_block_type'];

	switch($type) :
		case "text" :
			$textBlock = $item['text_block'];

			$column_layout = $textBlock['text_column_layout'];
			switch( $column_layout ) :
				case '1' :
					$columnClass = 'col-md-12';
				break;
				case '2' :
					$columnClass = 'col-md-6';
				break;
				case '3' :
					$columnClass = 'col-md-4';
				break;
				case '4' :
					$columnClass = 'col-md-3';
				break;
			endswitch;

			$textColumns = $textBlock['text_columns'];
?>
<section class="module copy <?php if ( $textBlock['text_css'] !== '' ) : echo $textBlock['text_css']; endif; ?>" <?php if ( $textBlock['text_has_background'] === 'yes' ) : echo "style=\"background-image:url('".$textBlock['text_background']."');\""; endif; ?>>

	<header>
	<?php if ( $textBlock['text_heading'] !== '' ) : ?>
	<h2><?php echo $textBlock['text_heading']; ?></h2>
	<?php endif; ?>
	</header>

	<div class="row">
		<?php foreach ($textColumns as $key => $textColumn) :
			$thisColumn = $textColumn['text_column'];
		?>
		<div class="<?php echo $columnClass; ?>">
			<div class="contentBox copyBox">
				<div>
					<?php if ( $thisColumn['text_column_heading'] !== '' ) : ?>
					<h3><?php echo $thisColumn['text_column_heading']; ?></h3>
					<?php endif; ?>
					<div class="copy">
						<?php echo $thisColumn['text_column_copy']; ?>
					</div>
				</div>
			</div>
		</div>
		<?php
		endforeach; ?>		
	</div>
</section>
<?php
		break;
		case "video" :
			$video = [
				'format' => $item['video_block']['video_format'],
				'source' => $item['video_block']['video_source'],
				'poster' => $item['video_block']['video_poster'],
				'label' => $item['video_block']['video_caption'],
				'start' => $item['video_block']['video_start'],
			];
			$videoIdentifier++;
?>
<section class="module fullwWidthVideo">
  <div class="row">
    <div class="videoPlayer" data-type="<?php echo $video['format']; ?>" data-source="<?php echo $video['source']; ?>" id="fwv<?php echo $videoIdentifier; ?>">
      <div class="poster"><img alt="" src="<?php echo $video['poster']; ?>"></div>
      <div class="video <?php echo $video['format']; ?>">
      <?php 
          switch($video['format']) {
            case 'youtube' : {
          ?>
          <iframe id="yt01" class="yt_player_iframe" src="https://www.youtube.com/embed/<?php echo $video['source']; ?>?enablejsapi=1&version=3&playerapiid=ytplayer&start=<?php echo $video['start']; ?>" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
          <?php
            } break;
            case 'vimeo' : {
          ?>
          <iframe src="https://player.vimeo.com/video/<?php echo $video['source']; ?>?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
          <?php
            } break;
            case 'ted' : {
          ?>
			<iframe src="https://embed.ted.com/talks/<?php echo $video['source']; ?>" style="width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe>
          <?php
            } break;
            case 'inline' : {
          ?>
          <video preload="metadata" controls="controls" playsinline="" noloop="">
            <source type="video/mp4" src="<?php echo $video['source']; ?>">
          </video>
          <?php
            } break;
          }
      ?>    
      </div>
      <?php if ( $video['label'] !== '' ) : ?><h2><?php echo $video['label']; ?></h2><?php endif; ?>
      <a href="" class="play"></a>
    </div>
  </div>  
</section>
<?php
		break;
	endswitch;
	endforeach;
endif;
?>

