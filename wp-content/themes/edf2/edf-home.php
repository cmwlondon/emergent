<?php
/*
Template Name: edf-home
*/

if ( have_posts() ) {
  the_post();

  /*
  retrieve ACF fields for page banenr
  page_banner_title
  page_banner_subtitlecopy
  page_banner_image
  page_banner_class
  */  
  $pageBannerMeta = [];

  // header: <head> tag, <body>, page navigation, search box, page hero/banner
  get_header( null, $pageBannerMeta ); 
  // get_template_part( 'blocks/carousel','',[]);

  // copy
  get_template_part( 'blocks/copy','',[]);

  // newsletter form
  get_template_part( 'blocks/newsletterSignUp','',[]);


  // alternating text/video or image blocks
  get_template_part( 'blocks/alternating','',[]);

  // backers module
  // apply class 'homeBackers'
  get_template_part( 'blocks/backers','',[]);

  // explore links
  get_template_part( 'blocks/explore','',[] );

  
  // tail
  /*
  page_tail_background
  page_tail_image
  page_tail_quote
  page_tail_speaker
  */
  $pageTailMeta = [
    'background_color_class' => get_field('page_tail_background'),
    'image' => get_field('page_tail_image'),
    'quote1' => get_field('page_tail_quote'),
    'quote2' => get_field('page_tail_speaker'),
    'showlogo' => 'no'
  ];
  get_template_part( 'blocks/tail','', $pageTailMeta );

  // footer
  // </body>
  get_footer( null, [] );
}
?>
