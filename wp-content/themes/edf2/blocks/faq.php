<?php
/*
faq_heading
faq_introduction_copy HTML -> DIV
faq_groups[
	faq_group[
		faq_group_heading
		faq_items[
			faq_item[
				faq_item_heading
				faq_item_copy HTML -> DIV
			]
		]
	]
]
*/

$faqGroups = get_field('faq_groups');
?>
<section class="module faqs width90percent">
	<?php if ( get_field('faq_heading') !== '' || get_field('faq_introduction_copy') !== '' ) : ?>
	<header>
		<?php if ( get_field('faq_heading') !== '' ) : ?>
			<h2><?php the_field('faq_heading'); ?></h2>
		<?php endif; ?>
		<?php if ( get_field('faq_introduction_copy') !== '' ) : ?>
		<div class="intro"><?php the_field('faq_introduction_copy'); ?></div>
		<?php endif; ?>
	</header>
	<?php endif; ?>


	<?php if ( count($faqGroups) > 0 ) : ?>
	<div class="row">
		<div class="col">
			<?php
			foreach ($faqGroups AS $groupIndex => $faqGroupBox) :
				$faqGroup = $faqGroupBox['faq_group'];
			?>
			<section class="faqGroup" data-group="fq<?php echo $groupIndex + 1; ?>">
				<header>
					<h3><?php echo $faqGroup['faq_group_heading']; ?></h3>
				</header>
				<div class="outer" id="faqgroup<?php echo $groupIndex + 1; ?>"">
					<div class="inner">
					<?php if ( count($faqGroup['faq_items']) > 0 ) : ?>

						<ol class="subfaqs">
						<?php
						foreach( $faqGroup['faq_items'] AS $itemIndex => $itemBox ) :
							$item = $itemBox['faq_item'];
						?>
							<li>
								<article class="subfaq">
									<header class="question">
										<h4><span><?php printf("%02d", ( $itemIndex + 1 )); ?>.</span><?php echo $item['faq_item_heading']; ?></h4>
									</header>
									<div class="outer" id="faq-g<?php echo $groupIndex + 1; ?>q<?php echo $itemIndex + 1; ?>">
										<div class="inner">
											<?php echo $item['faq_item_copy']; ?>
										</div>
									</div>
								</article>
							</li>
						<?php endforeach; ?>
						</ol>
					<?php endif; ?>
					</div>
				</div>
			</section>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>	
</section>