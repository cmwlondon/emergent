<?php
/* choices3 */
/*
ACF: 'Choices'
  intro-title
  intro-copy
  dividers 'no' / 'yes'
  choices_dynamic 'no' / 'yes'
  columns[
    column[
      icon id
      heading
      copy
      has_a_link 'no' / 'yes'
      link[
      ]
      has_box_background_image 'no' / 'yes'
      box_background_image url
    ]
  ]
*/
?>
<section class="module choices3">
  <header>
    <h2><?php echo get_field('intro-title'); ?></h2>
    <?php if (get_field('intro-copy') !== '') : ?><p><?php echo get_field('intro-copy'); ?></p><?php endif;?>
  </header>
  <div class="row">
    <?php
    if( have_rows('columns') ):

      while( have_rows('columns') ) :
        the_row();
        $item = get_sub_field('column');
        
    ?>
        <div class="col-lg-4">
          <div class="boxo">
            <div class="boxi">
              <?php if( $item['icon'] ) : ?><figure><?php echo wp_get_attachment_image( $item['icon'], 'full' ); ?></figure><?php endif; ?>
              <div class="relflex">
                <div class="copy">
                  <h3><?php echo $item['heading']; ?></h3>
                  <div><?php echo $item['copy']; ?></div>
                </div>
              </div>
            </div>
            <?php if ( $item['has_box_background_image'] === 'yes' ) : ?><div class="boxii" style="background-image: url('<?php echo $item['box_background_image']; ?>');"></div><?php endif; ?>
          </div>    
        </div>
    <?php
      endwhile;
    endif;
    ?>

  </div>
</section>
