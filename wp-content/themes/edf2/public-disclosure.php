<?php
/*
Template Name: public-disclosure
*/

$pageBannerMeta = [
  'page_banner_class' => get_field('page_banner_class'),
  'page_banner_title' => get_field('page_banner_title'),
  'page_banner_copy' => get_field('page_banner_subtitlecopy')
];
get_header( null, $pageBannerMeta );

// page content
/**/
?>
<!-- cookie policy START -->
<a href="https://www.iubenda.com/privacy-policy/26866108/cookie-policy" class="iubenda-white no-brand iubenda-embed iub-body-embed" title="Cookie Policy">Cookie Policy</a>
<script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
<!-- cookie policy END -->
<?php
/**/

$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
