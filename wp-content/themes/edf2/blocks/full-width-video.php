<?php
/*
ACF 'Full width videp'
fwv_format -> 'youtube' / 'vimeo' / 'ted' / 'inline'
fwv_source
fwv_poster -> image URL
fwv_label
fwv_start

  'type' => 'youtube',
  'source' => 'f2n7au0E-Y4',
  'poster' => get_template_directory_uri().'/images/how-we-work/video-poster.jpg',
  'title' => ''
*/

  $item = [
    'format' => get_field('fwv_format'),
    'source' => get_field('fwv_source'),
    'poster' => get_field('fwv_poster'),
    'label' => get_field('fwv_label'),
    'start' => get_field('fwv_start'),
  ];
?>
    <section class="module fullwWidthVideo">
      <div class="row">
        <div class="videoPlayer" data-type="<?php echo $item['format']; ?>" data-source="<?php echo $item['source']; ?>" id="fwv01">
          <div class="poster"><img alt="" src="<?php echo $item['poster']; ?>"></div>
          <div class="video <?php echo $item['format']; ?>">

          <?php 
              switch($item['format']) {
                case 'youtube' : {
              ?>
              <iframe id="yt01" class="yt_player_iframe" src="https://www.youtube.com/embed/<?php echo $item['source']; ?>?enablejsapi=1&version=3&playerapiid=ytplayer&start=<?php echo $item['start']; ?>" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
              <?php
                } break;
                case 'vimeo' : {
              ?>
              <iframe src="https://player.vimeo.com/video/<?php echo $item['source']; ?>?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              <?php
                } break;
                case 'ted' : {
              ?>
              <?php
                } break;
                case 'inline' : {
              ?>
              <video preload="metadata" controls="controls" playsinline="" noloop="">
                <source type="video/mp4" src="<?php echo $item['source']; ?>">
              </video>
              <?php
                } break;
              }
          ?>    

          </div>
          <h2><?php echo $item['label']; ?></h2>
          <a href="" class="play"></a>
        </div>
      </div>  
    </section>
