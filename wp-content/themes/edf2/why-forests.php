<?php
/*
Template Name: why-forests
*/

if ( have_posts() ) {
  the_post();
  $page_id = $post->ID;

  $pageBannerMeta = [
    'page_banner_class' => get_field('page_banner_class'),
    'page_banner_title' => get_field('page_banner_title'),
    'page_banner_copy' => get_field('page_banner_subtitlecopy')
  ];
  get_header( null, $pageBannerMeta );

  // two column copy repeater
  // get_template_part( 'blocks/copy-repeater','',[]);

?>

<section class="module alternatingPairs fe1">
  <div class="row dg vr" id="row1">
    <div class="col-lg-7 vp">
      <div class="staticImage"><figure><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/04/birds1.jpg"></figure></div>
    </div>
    <div class="col-lg-5 caption align-items-center">
      <div class="captionInner">
        <div class="captionEvenInner">
          <h3>Why Forests?</h3>
          <div>
            <ul>
              <li>Tropical forests contain 50% of all plants and animals on the earth’s landmasses, and support the livelihoods of up to 1.6 billion people.</li>
              <li>Only 1% of the rainforest’s plant and animal species have been studied, meaning there are boundless opportunities for new medicines and discoveries.</li>
              <li>Rainforests are thought to store at least 250 billion tons of carbon, equivalent to over 200 years of human-induced carbon emissions.</li>
              <li>A single bush in the Amazon may have more species of ants than the entire British Isles.</li>
              <li>A tropical rainforest may have more than 480 tree species in a single hectare, compared with temperate forests where just six species may make up 90 percent of the trees in an entire forest.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="module copy width70percent limitPara bgNoRepeat bgRightTop" style="background-image:url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/tree-bg4.png');">
<header>
<h2>Enabling Forest Protection</h2>
</header>
<div class="row">
<div class="col-md-12">
<div class="contentBox copyBox">
<div>
<div class="copy">
<p>Forests are important to climate and crucial to life on earth. Protecting them represents perhaps the most promising, large-scale, and cost-effective opportunity to manage climate change, with the potential to contribute up to a third of the solution.</p>
<p>Emergent aims to make forests worth more alive than dead by channeling private sector action to reverse tropical deforestation.</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="module fullwWidthVideo">
  <div class="row">
    <div class="videoPlayer" data-type="ted" data-source="eron_bloomgarden_a_new_financial_model_for_conservation" id="fwv1">
      <div class="poster"><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/03/eron_bloomgarden_a_new_financial_model_for_conservation2.jpg"></div>
      <div class="video ted">
        <iframe src="https://embed.ted.com/talks/eron_bloomgarden_a_new_financial_model_for_conservation" style="width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe>
      </div>
      <a href="" class="play"></a>
    </div>
  </div>  
</section>

<section class="module copy width70percent">
  <header>
    <h2>Mobilizing At Scale</h2>
  </header>
  <div class="row">
    <div class="col-md-12">
      <div class="contentBox copyBox">
        <div>
          <div class="copy">
            <p>The urgent climate crisis requires an accelerated model of change to slow and reverse deforestation. This is why Emergent focuses on large-scale mobilization of finance for national and jurisdictional-level REDD+ programs.</p>
            <p>Building on the international climate change frameworks under the <a href="https://unfccc.int/topics/land-use/resources/warsaw-framework-for-redd-plus" rel="noopener" target="_blank">Warsaw Framework for REDD+</a>, the <a href="https://redd.unfccc.int/fact-sheets/safeguards.html" rel="noopener" target="_blank">Cancun Safeguards</a>, and the <a href="https://unfccc.int/process-and-meetings/the-paris-agreement/the-paris-agreement" rel="noopener" target="_blank">Paris Agreement</a>, Emergent provides a market financing and transactional facility to drive large-scale demand and supply of high-quality international emissions units.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php
  // tail
  /*
  page_tail_background
  page_tail_image
  page_tail_quote
  page_tail_speaker
  */
  $pageTailMeta = [
    'background_color_class' => get_field('page_tail_background'),
    'image' => get_field('page_tail_image'),
    'quote1' => get_field('page_tail_quote'),
    'quote2' => get_field('page_tail_speaker')
  ];
  get_template_part( 'blocks/tail','', $pageTailMeta );

  get_footer();
}
?>
