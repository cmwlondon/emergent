<?php
/*
ACF Pro
Custom Fields:
'Explore links'
  links[
    link[
      title
      url
      image (image ID)
    ],
    ...
  ]
*/
?>
    <section class="module exploreLinks">
      <div class="row">

        <?php
        if( have_rows('links') ):

          while( have_rows('links') ) :
            the_row();
            $item = get_sub_field('link');
        ?>
        <div class="col-md-4">
          <div class="inner">
            <div class="frame"><?php echo wp_get_attachment_image( $item['image'], 'full',false, ['alt' => $item['title']]); ?></div>
            <div class="label">
              <h2><?php echo $item['title']; ?></h2>

            </div>
            <a class="action" title="<?php echo $item['title']; ?>" href="<?php echo $item['url']; ?>"></a>
          </div>
        </div>          

        <?php
          endwhile;
        endif;
        ?>
      </div>
    </section>
