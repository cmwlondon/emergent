    <section class="module tail no-image no-caption">
      <div class="background">
        <div class="wave">
          <img class="ablogo" src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch/logo-x.png">
          <img class="curve" src="<?php echo get_template_directory_uri(); ?>/images/get-in-touch/curve.png">
          <div class="message">
            <h2>Get in touch</h2>
            <p>If you have any questions about our high-integrity carbon credits, please don’t hesitate to email us at <a href="mailto:info@emergentclimate.com">info@emergentclimate.com</a>.</p>
          </div>
        </div>
      </div>
    </section>
