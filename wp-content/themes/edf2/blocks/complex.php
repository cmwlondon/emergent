<?php
/*
three steps / complex
ACF: 'Three steps'

three_steps_heading
three_steps_blocks[
  three_steps_block[
    three_steps_block_heading
    three_steps_block_copy
    three_steps_block_icon url
  ]
]
*/
$blocks = get_field('three_steps_blocks');
?>
<section class="module complex">
  <header>
    <h2><?php the_field('three_steps_heading'); ?></h2>
  </header>
  <div class="row">

    <?php
    if ( count($blocks) > 0 ) :
      foreach($blocks AS $blockIndex => $block) :
      $item = $block['three_steps_block'];
    ?>
    <div class="col-lg-4 c<?php echo ($blockIndex + 1); ?>" style="background-image:url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/line-<?php echo ($blockIndex + 1); ?>a.png');">
      <figure class="order"><img alt="1" src="<?php echo site_url(); ?>/wp-content/uploads/2021/03/complex-<?php echo ($blockIndex + 1); ?>.png"></figure>
      <div class="boxi" <?php if ($blockIndex % 3 === 1) : ?>style="background-image:url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/c-fill.png');"<?php endif; ?>>
        <div class="copy">
          <h3><?php _e($item['three_steps_block_heading']); ?></h3>
          <figure><img alt="" src="<?php echo $item['three_steps_block_icon']; ?>"></figure>
          <p><?php _e($item['three_steps_block_copy']); ?></p>
        </div>
      </div>
    </div>

  <?php
    endforeach;
  endif;
  ?>
  </div>  
</section>
