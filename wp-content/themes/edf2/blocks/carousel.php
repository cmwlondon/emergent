<?php
// carousel
// source/js/common.js
/*
carousel_speed (milliseconds) min 300ms
carousel_direction left/right
carousel_animation slide/fade
carousel_mode auto/manual
carousel_items[
	carousel_item[
		carousel_item_image URL
		carousel_item_heading
		carousel_item_copy
	]
]
*/
/*
data-speed="1000"
data-direction="left" left/right
data-animation="slide"
data-mode="auto" auto/manual
data-has-controls="yes" yes/no
data-has-index="yes" yes/no
data-slides=".item"
*/

$carousel = [
	'speed' => get_field('carousel_speed'),
	'direction' => get_field('carousel_direction'),
	'animation' => get_field('carousel_animation'),
	'mode' => get_field('carousel_mode'),
	'items' => get_field('carousel_items')
];

$items = [
	[
		'image' => site_url().'/wp-content/uploads/2021/03/Soil-Carbon-scaled-1.jpeg',
		'header' => 'A breakthrough in forest protection',
		'copy' => 'The highest integrity emissions reduction. At an unprecedented scale.'
	],
	[
		'image' => site_url().'/wp-content/uploads/2021/03/carousel-2.jpg',
		'header' => 'Providing access to the highest integrity credits',
		'copy' => ''
	],
	[
		'image' => site_url().'/wp-content/uploads/2021/03/carousel-3.jpg',
		'header' => 'Preserving earth’s biodiversity',
		'copy' => ''
	],
	[
		'image' => site_url().'/wp-content/uploads/2021/03/carousel-4.jpg',
		'header' => 'Supporting global local communities',
		'copy' => ''
	]
];
?>
<section class="module carousel">
	<div class="inca" data-speed="<?php echo $carousel['speed']; ?>" data-direction="<?php echo $carousel['direction']; ?>" data-animation="<?php echo $carousel['animation']; ?>" data-mode="<?php echo $carousel['mode']; ?>" data-has-controls="yes" data-has-index="no" data-slides=".item">
		<div class="items">

			<?php
			foreach( $carousel['items'] AS $key => $item ) :
				$slide = $item['carousel_item'];
				$image = $slide['carousel_item_image'];
				$heading = $slide['carousel_item_heading'];
				$copy = $slide['carousel_item_copy'];
			?>
			<div class="item mutex" id="h<?php echo $key ?>">
				<div class="row">
					<div class="col-md-4 image">
						<img alt="" src="<?php echo $image; ?>">
					</div>
					<div class="col-md-8 copy">
						<div>
							<h3><?php echo $heading; ?></h3>
<?php if ( $copy !== '' ) : ?><p><?php echo $copy; ?></p><?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>

		</div>	

		<div class="control left" data-action="previous">
			<div><img src="<?php echo get_template_directory_uri(); ?>/images/misc/previous.png" alt="previous"></div>
		</div>

		<div class="control right" data-action="next">
			<div><img src="<?php echo get_template_directory_uri(); ?>/images/misc/next.png" alt="next"></div>
		</div>

		<!--
		<div class="index">
			<ul>
<?php foreach( $items AS $key => $item ) : ?>
<li><?php echo ($key + 1); ?></li>
<?php endforeach ?>				
			</ul>
		</div>
		-->

	</div>
	
</section>