<?php
/*
ACF custom field group 'Boardmembers'
board_title
board_introduction
boardmembers[
  boardmember -> post->ID
]

boardmember -> custom post type 'boardmembers'
board_name
board_role
*/

$board_members = get_field('boardmembers');
$boardTitle = word_wrapper(get_field('board_title'));
$boardIntroduction = ( get_field('board_introduction') !== '' ) ? word_wrapper(get_field('board_introduction')) : '';
?>

    <section class="module team boardmembers width90percent">
      <header>
        <h2><?php _e($boardTitle); ?></h2>
        <?php if ( $boardIntroduction !== '') : ?><p><?php _e($boardIntroduction); ?></p><?php endif; ?>
      </header>

      <div class="row">
<?php
if ($board_members) :
  foreach($board_members AS $board_index => $item) :
    $board_member_id = $item['boardmember'];
    $photo = get_field('board_photo', $board_member_id);
    $name = get_field('board_name', $board_member_id);
    $role = get_field('board_role', $board_member_id);
?>

        <div class="col-sm-4">
          <div class="copybox">
            <?php if ( $photo ) : ?>
            <figure><img alt="<?php _e($name); ?>" src="<?php echo $photo; ?>"></figure>
            <?php endif; ?>

            <h3><?php _e($name); ?></h3>
            <h4><?php _e($role); ?></h4>
          </div>
        </div>
<?php
  endforeach;
endif;
?>
      </div>
    </section>
