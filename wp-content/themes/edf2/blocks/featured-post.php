<?php
// featured-post
/*
ACF 'Emergent Blog Post'
blog_title
blog_synopsis
thumbnail
blog_has_a_video
blog_video[
  blog_video_format
  blog_video_poster
  blog_video_caption
  blog_video_source
]
blog_image[]
*/
// echo "<pre>".print_r(get_field('blog_image'),true)."</pre>";
$blogImageData = get_field('blog_image');
$blogImageURL = $blogImageData['url'];
$hasVideo = get_field('blog_has_a_video');
$date = get_the_date('', $omitID);
$authorID = $post->post_author;
$author = get_user_by('ID',$authorID);
$authorName = $author->data->user_nicename;

?>
    <section class="module featuredPost">
      <div class="row" id="ebp<?php echo $omitID; ?>">

        <div class="col-md-8 latestThumbmnail">

          <?php
          
          echo "<pre></pre>";
          ?>
<?php
switch( $hasVideo ) :
case 'yes' : {
  $item = get_field('blog_video');
?>
        <div class="videoPlayer" data-type="<?php echo $item['blog_video_format']; ?>" data-source="<?php echo $item['blog_video_source']; ?>" id="fwv01">
          <div class="poster"><img alt="" src="<?php echo $item['blog_video_poster']; ?>"></div>
          <div class="video <?php echo $item['blog_video_format']; ?>">

          <?php 
              switch($item['blog_video_format']) {
                case 'youtube' : {
              ?>
              <iframe id="yt01" class="yt_player_iframe" src="https://www.youtube.com/embed/<?php echo $item['blog_video_source']; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
              <?php
                } break;
                case 'vimeo' : {
              ?>
              <iframe src="https://player.vimeo.com/video/<?php echo $item['blog_video_source']; ?>?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              <?php
                } break;
                case 'ted' : {
              ?>
              <?php
                } break;
                case 'inline' : {
              ?>
              <video preload="metadata" controls="controls" playsinline="" noloop="">
                <source type="video/mp4" src="<?php echo $item['blog_video_source']; ?>">
              </video>
              <?php
                } break;
              }
          ?>    

          </div>
          <?php // echo "<h2>".$item['blog_video_caption']."</h2>"; ?>
          <a href="" class="play"></a>
        </div>
<?php
} break;
case 'no' : {
?>
<figure><img alt="<?php the_field('blog_title'); ?>" src=" <?php echo $blogImageURL; ?>"></figure>
<?php  
} break;
endswitch;
?>

        </div>
        <div class="col-md-4 latestInfo">
          <div class="copybox ">
            <p>posted by: <?php echo $authorName; ?> on <?php echo $date; ?></p>
            <h2><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_field('blog_title'); ?></a></h2>
            <p><?php the_field('blog_synopsis'); ?></p>
            <a href="<?php echo get_permalink(get_the_ID()); ?>">Find out more</a>
          </div>
        </div>
        
      </div>
    </section>
