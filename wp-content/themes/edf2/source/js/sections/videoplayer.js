function VideoPlayer(parameters) {
  this.parameters = parameters;

  this.init();
}

VideoPlayer.prototype = {
  "constructor" : VideoPlayer,
  "template" : function () {var that = this; },
  
  "init" : function () {

    let that = this;

    if ( jQuery('.videoPlayer').length > 0 ) {

      this.videoNode = null;
      this.currentRunningVid = {
        "id": "0",
        "type" : "0",
        "src" : "0"
      };

      // bind behaviour to video 'play' overlays
      jQuery('.videoPlayer a.play').on('click',function(e){
        e.preventDefault();
        that.videoAction(jQuery(this));
      });
    }

  },

  "videoAction" : function(videoTrigger) {
      let vp = videoTrigger.parents('.videoPlayer');

      console.log("currently running - type: '%s'", this.currentRunningVid.type);

      if(this.currentRunningVid.id !== "0"){
        // stop running video
        switch( this.currentRunningVid.type ) {

          case 'youtube' : {
            this.stopYoutube();
          } break;

          case 'vimeo' : {
            this.stopVimeo();
          } break;

          case 'ted' : {
            this.stopVimeo();
          } break;

          case 'inline' : {
            this.stopInline();
          } break;
        }
      } else {
        console.log('MESSAGE: no video running;');
      }

      // close any open panels
      jQuery('.videoPlayer.open').removeClass('open');

      // collect information on selected video element
      let vID = vp.attr('id');
      let vType = vp.attr('data-type');
      let vSource = vp.attr('data-source');

      console.log("starting type: '%s'", vType);

      this.currentRunningVid = {
        "id" : vID,
        "type" : vType,
        "src" : vSource
      };


      // autostart videos
      switch( vType ) {
          case 'youtube' : {
            this.startYoutube(vp);
          } break;

          case 'vimeo' : {
            this.startVimeo(vp);
          } break;

          case 'inline' : {
            this.startInline(vp);
          } break;
      }
      // open selected panel
      vp.addClass('open');
  },

  "startYoutube" : function(target) {
      let ytIframe = target.find('iframe').eq(0).get(0);
      ytIframe.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
      this.videoNode = null;  
  },

  "startVimeo" : function(target) {
      console.log('autostart vimeo');
      this.videoNode = null;
  },

  "startInline" : function(target) {
      // start video if it has already been inserted into the page
      let tvt = target.find('video').eq(0);
      tvt.get(0).play();
      this.videoNode = tvt;
  },

  "stopYoutube" : function() {
    let ytIframe = jQuery('#' + this.currentRunningVid.id + ' iframe').get(0);
    ytIframe.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
  },

  "stopVimeo" : function() {
    console.log("stop vimeo video:'%s", this.currentRunningVid.src);
  },

  "stopInline" : function() {
    this.videoNode.get(0).pause();
    this.videoNode.get(0).currentTime = 0;
  },

/* 
  // methods to insert new iframes/inline video tags
  
  "insertYoutubeIframe" : function(target,source) {
      // insert youtube iframe with autoplay property
      let iframe = jQuery('<iframe></iframe>');
      iframe.attr({
        "src" : "https://www.youtube.com/embed/" + source + "?autoplay=1&enablejsapi=1&version=3&playerapiid=ytplayer",
        "allow" : "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
        "allowfullscreen" :"true",
        "allowscriptaccess" : "always",
        "frameborder" : "0"
      });
      
      target.append(iframe);
  },

  "insertVimeoIframe" : function(target,source) {
    console.log('insert vimeo iframe');
  },

  "insertInlineVideo" : function(target,source) {
    if (target.find('video').length !== 0) {
      // start video if it has already been inserted into the page
      let tvt = target.find('video').eq(0);
      tvt.get(0).play();
      this.videoNode = tvt;
    } else {
      // insert new video tag
      let vt = jQuery('<video></video>');
      vt.attr({
        "preload" : "metadata",
        "controls" : "controls",
        "playsinline" : "",
        "noloop" : ""
      });
      let vtsrc = jQuery('<source></source>');
      vtsrc.attr({
        "type" : "video/mp4",
        "src" : "public/video/" + source
      });

      vt.on('loadedmetadata',function(){
        let video = jQuery(this).get(0),
        readyInfo = video.readyState;
          window.setTimeout(function() {
            video.play();
          }, )
      });

      vt.append(vtsrc);
      target.append(vt);

      this.videoNode = vt;
    }
  }
  */
}

export default VideoPlayer;

