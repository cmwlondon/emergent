    <section class="module alternatingPairs">
      <header>
        <h2>We exist to end tropical deforestation</h2>
      </header>
        <div class="row lg vl">
          <div class="col-sm-7 vp">
          <div class="videoPlayer" data-type="youtube" data-source="ibJ4qNzz8gc" id="vp01">
            <div class="poster"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/home/posters/video01.jpg"></div>
            <div class="video youtube">
              <iframe id="yt01" class="yt_player_iframe" src="https://www.youtube.com/embed/ibJ4qNzz8gc?enablejsapi=1&version=3&playerapiid=ytplayer" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>

            </div>
            <a href="" class="play"></a>
          </div>
          </div>
          <div class="col-sm-5 caption align-items-center">
            <div class="captionInner">
              <h3>The need for forests</h3>
              <p>The science clearly shows protecting forests is the best single opportunity to slow down and ultimately halt climate change. Deforestation contributes 13% of global emission, more than any single country outside the US and China.</p>
              <a href="">Find out more</a>
            </div>
          </div>
        </div>
        <div class="row dg vr">
          <div class="col-sm-7 vp">
            <div class="videoPlayer" data-type="vimeo" data-source="78535150" id="vp02">
              <div class="poster"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/home/posters/video02.jpg"></div>
              <div class="video vimeo">
                <iframe id="vi01" src="https://player.vimeo.com/video/78535150?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              </div>
              <a href="" class="play"></a>
            </div>
          </div>
          <div class="col-sm-5 caption">
            <div class="captionInner">
              <h3>The need for forests</h3>
              <p>The science clearly shows protecting forests is the best single opportunity to slow down and ultimately halt climate change. Deforestation contributes 13% of global emission, more than any single country outside the US and China.</p>
              <a href="">Find out more</a>
            </div>
          </div>
        </div>
        <div class="row lg vl">
          <div class="col-sm-7 vp">
            <div class="videoPlayer" data-type="inline" data-source="sample.mp4"  id="vp03">
              <div class="poster"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/home/posters/video03.jpg"></div>
              <div class="video inline">
                <video preload="metadata" controls="controls" playsinline="" noloop="" id="in01">
                  <source type="video/mp4" src="<?php echo get_template_directory_uri(); ?>/video/sample.mp4">
                </video>
              </div>
              <a href="" class="play"></a>
            </div>
          </div>
          <div class="col-sm-5 caption">
            <div class="captionInner">
              <h3>The need for forests</h3>
              <p>The science clearly shows protecting forests is the best single opportunity to slow down and ultimately halt climate change. Deforestation contributes 13% of global emission, more than any single country outside the US and China.</p>
              <a href="">Find out more</a>
            </div>
          </div>
        </div>
        <div class="row dg vr">
          <div class="col-sm-7 vp">
            <div class="videoPlayer" data-type="inline" data-source="sample.mp4" id="vp04">
              <div class="poster"><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/home/posters/video04.jpg"></div>
              <div class="video inline">
                <video preload="metadata" controls="controls" playsinline="" noloop="" id="in01">
                  <source type="video/mp4" src="<?php echo get_template_directory_uri(); ?>/video/sample.mp4">
                </video>
              </div>
              <a href="" class="play"></a>
            </div>
          </div>
          <div class="col-sm-5 caption">
            <div class="captionInner">
              <h3>The need for forests</h3>
              <p>The science clearly shows protecting forests is the best single opportunity to slow down and ultimately halt climate change. Deforestation contributes 13% of global emission, more than any single country outside the US and China.</p>
              <a href="">Find out more</a>
            </div>
          </div>
        </div>
    </section>
