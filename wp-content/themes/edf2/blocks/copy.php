<?php
/*
ACF 'Column copy'
multi column copy block: 1,2 or 3 columns

two formats
a) header / copy / link
b) quote / source

two_column_classes
two_col_dividers
two_col_heading
two_col_introduction
number_of_columns 1, 2, 3

two_col_content[
  two_col_block[
    two_col_block_format 'text','quote'
    'text'
    heading_text_link_layout[
      two_col_block_heading
      two_col_block_text
      two_col_block_haslink 'no','yes'
      two_col_block_link[
        link_text
        link_url
        external_link  'no','yes'
      ]
    ]
    'quote'
    two_col_quote[
      quote_quote
      quote_source
    ]
  ],
  ...
]
]

number_of_columns:
1 -> '.oneColumn .col'
2 -> '.twoColumn .col-md-6'
3 -> '.threeColumn .col_md-4'
*/
$blocks = get_field('two_col_content');
$numberOfColumns = get_field('number_of_columns');
$classes = [
  '1' => [
    'parent' => 'oneColumn',
    'column' => 'col-md-12'
  ],
  '2' => [
    'parent' => 'twoColumn',
    'column' => 'col-md-6'
  ],
  '3' => [
    'parent' => 'threeColumn',
    'column' => 'col-md-4'
  ]
];
?>
    <section class="module copy <?php echo $classes[$numberOfColumns]['parent']; ?> <?php the_field('two_column_classes'); ?> <?php if (get_field('two_col_dividers') === 'yes') : ?>dividers<?php endif; ?>">
<?php
  if ( get_field('two_col_heading') !== '' || get_field('two_col_introduction') !== '' ) :
?>
      <header>
<?php
if ( get_field('two_col_heading') !== '' ) :
?>  
        <h2><?php the_field('two_col_heading'); ?></h2>
<?php
endif;
?>
<?php
if ( get_field('two_col_introduction') !== '' ) :
?>  
        <p><?php the_field('two_col_introduction'); ?></p>
<?php
endif;
?>
      </header>
<?php
endif;
?>
      <div class="row">

<?php 

foreach($blocks AS $index => $blockx) :
  $block = $blockx['two_col_block'];

  // get format
  $format = $block['two_col_block_format'];

  switch($format) :
    case 'text' : 
      $textblock = $block['heading_text_link_layout'];
      $textLink = [];
      if ( $textblock['two_col_block_haslink'] === 'yes' ) :
        $textLink = $textblock['two_col_block_link'];
      endif;
  ?>
        <div class="<?php echo $classes[$numberOfColumns]['column']; ?>">
          <div class="contentBox copyBox">
            <div>
              <?php if ($textblock['two_col_block_heading'] !== '' ) : ?><h2><?php echo $textblock['two_col_block_heading']; ?></h2><?php endif; ?>
              <?php if ($textblock['two_col_block_text'] !== '' ) : ?><p><?php echo $textblock['two_col_block_text']; ?></p><?php endif; ?>
              <?php if ( $textblock['two_col_block_haslink'] === 'yes' ) : ?>
              <a href="<?php echo $textLink['link_url']; ?>" class="stickToBottom" <?php if ( $textLink['external_link'] === 'yes' ) : ?>target="_blank"<?php endif; ?> ><?php echo $textLink['link_text']; ?></a>
              <?php endif; ?>
            </div>
          </div>
        
        </div>
  <?php
    break;
    case 'quote' : 
      $quoteblock = $block['two_col_quote']
  ?>
        <div class="col-md-6">
          <div class="contentBox quoteBox">
            <div>
              <blockquote>&ldquo;<?php echo $quoteblock['quote_quote']; ?>&rdquo;</blockquote>
              <p><?php echo $quoteblock['quote_source']; ?></p>
            </div>
          </div>
        </div>
  <?php
    break;
  endswitch;
  /*
  if ($block['two_col_block_has_a_link'] === 'yes') :
    $link = [
      'label' => $block['two_col_block_link']['two_col_block_link_label'],
      'url' => $block['two_col_block_link']['two_col_block_link_url'],
      'external' => $block['two_col_block_link']['two_col_block_link_external_link']
    ];
  endif;
  */
?>

<?php 
endforeach;
?>

      </div>
    </section>
