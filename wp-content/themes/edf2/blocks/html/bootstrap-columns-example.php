    <div class="container">
      <div class="row">
        <div class="col-sm one lg">one</div>
        <div class="col-sm two dg">two</div>
        <div class="col-sm three lg">three</div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-4 one lg">one</div>
        <div class="col-sm-8 two dg">two</div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-8 two dg">two</div>
        <div class="col-sm-4 one lg">one</div>
      </div>
    </div>
