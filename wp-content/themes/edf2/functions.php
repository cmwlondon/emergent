<?php
/*
edf2 theme
functions 
*/

ini_set( 'mysql.trace_mode', 0 );


// create custom post types for team members and board members
function create_posttype() {
 
    register_post_type( 'team_members',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Our Team' ), // label that appears in dasboard navigation
                'singular_name' => __( 'Team Member' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'team'), // site/team/NNN
            'show_in_rest' => true,
 
        )
    );

    register_post_type( 'board_members',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Our Board Members' ), // label that appears in dasboard navigation
                'singular_name' => __( 'Board Member' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'board'), // site/board/NNN
            'show_in_rest' => true,
 
        )
    );

}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );



// add title tag to page
function theme_slug_setup() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_slug_setup' );


// add arbitrary 'type' attribute to <script> tag based on enqueue identifier
function add_type_attribute($tag, $handle, $src) {
    // if not your script, do nothing and return original $tag
    if ( 'theme-js' !== $handle ) {
        return $tag;
    }
    // change the script tag by adding type="module" and return it.
    $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    return $tag;
}


/* add external fonts links to <head> */
function wpb_add_google_fonts() {
  wp_enqueue_style( 'wpb-fira-sans', 'https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap', false ); 
  wp_enqueue_style( 'wpb-neuzeit-grotesk', 'https://use.typekit.net/wkm0oox.css', false ); 
  
}
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );


/* add CSS styles to <head>*/
function themebs_enqueue_styles() {

  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
  wp_enqueue_style( 'core', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/css/theme.min.css' );

}
add_action( 'wp_enqueue_scripts', 'themebs_enqueue_styles');

/* javascript  - add theme.min/js just before </body> tag */
function themebs_enqueue_scripts() {
	// attach script just before closing body tag (last parameter in wp_enqueue_script() set to true)
	// wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/js/theme.min.js', array( 'jquery' ), false, true );

  // add filter to load theme.min.js as module
  /* ******** DOES NOT WORK IN IE11 ********
	add_filter('script_loader_tag', 'add_type_attribute' , 10, 4);
  */
}
add_action( 'wp_enqueue_scripts', 'themebs_enqueue_scripts');

function word_wrapper($text,$minWords = 3) {
   $return = trim($text);
   $arr = explode(' ',$text);
   if(count($arr) >= $minWords) {
           $arr[count($arr) - 2].= '&nbsp;'.$arr[count($arr) - 1];
           array_pop($arr);
           $return = implode(' ',$arr);
   }
   return $return;
}

?>

