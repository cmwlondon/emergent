<?php
/*
Template Name: how-it-works
*/

// get page data from ACF here

/*
get_header( null, [
  'page_banner_class' => '',
  'page_banner_title' => 'How we work',
  'page_banner_copy' => 'We enable transactions of high-integrity carbon credits from national-scale forest protection programmes.',
  'page_banner_image' => get_template_directory_uri().'/images/how-we-work/header.jpg'
] );
*/
$pageBannerMeta = [
  'page_banner_class' => get_field('page_banner_class'),
  'page_banner_title' => get_field('page_banner_title'),
  'page_banner_copy' => get_field('page_banner_subtitlecopy')
];
get_header( null, $pageBannerMeta );


/*
http://local.edf/wp-content/uploads/2021/03/line-1.png
http://local.edf/wp-content/uploads/2021/03/line-2.png
http://local.edf/wp-content/uploads/2021/03/line-3.png

http://local.edf/wp-content/uploads/2021/03/line-1a.png
http://local.edf/wp-content/uploads/2021/03/line-2a.png
http://local.edf/wp-content/uploads/2021/03/line-3a.png

http://local.edf/wp-content/uploads/2021/03/complex-1.png
http://local.edf/wp-content/uploads/2021/03/complex-2.png
http://local.edf/wp-content/uploads/2021/03/complex-3.png
*/
// complex
get_template_part( 'blocks/complex','',[]);

// choices
get_template_part( 'blocks/choices3','',[]);

// full width video
get_template_part( 'blocks/full-width-video','',[]);

// two column copy repeater
get_template_part( 'blocks/copy-repeater','',[]);

// tail
$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
