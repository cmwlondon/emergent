<?php
/*
Template Name: news-and-views-master
*/

$pageBannerMeta = [
  'page_banner_class' => get_field('page_banner_class'),
  'page_banner_title' => get_field('page_banner_title'),
  'page_banner_copy' => get_field('page_banner_subtitlecopy')
];
get_header( null, $pageBannerMeta );

$querySettings = [
  'post_type' => ['post'],
  'post_status' => ['publish'],
  'posts_per_page' => 1,
];
$featuredblogposts = new WP_Query($querySettings);

/*
  wp_query()
  https://developer.wordpress.org/reference/classes/wp_query/#post-page-parameters

  // display post(s) using list of IDs
  'post__in' => [329,331]

  // display post(s) NOT in list of IDs
  'post__not_in' => [331]

  // display single post
  'p' => 331

  // offset = skip over most recent post(s)
  'offset' => 1

  // display six posts per page, skip over first 2 pages (12 most recent posts)
  'posts_per_page' => 6,
  'paged' => 3

  // get current page based on URL parameter 'paged'
  'paged' => get_query_var( 'paged' )
*/

// get latest post
while ( $featuredblogposts->have_posts() ) :
  $featuredblogposts->the_post();
  $omitID = get_the_id();

  get_template_part( 'blocks/featured-post','', [] );

endwhile;
wp_reset_postdata(); 

// get older posts
get_template_part( 'blocks/older-posts-grid','', ['omitpost' => $omitID ] );

// tail
$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
