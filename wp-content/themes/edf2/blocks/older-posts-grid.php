<?php
// older-posts-grid

// $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; // works with /news-views/page/N/
$querySettings = [
  'post_type' => ['post'],
  'post_status' => ['publish'],
  'posts_per_page' => 6,
  'paged' => $paged,
  'post__not_in' => [ $args['omitpost'] ] // skip over featured post
];
$blogposts = new WP_Query($querySettings);
$maxPages = $blogposts->max_num_pages;

if ( $blogposts->have_posts() ) :

?>
    <section class="module olderPostsGrid">
<?php
	get_template_part( 'blocks/post-paging','', ['maxpages' => $maxPages,'paged' => $paged] );
?>

      <div class="row">
<?php
  // Load posts loop.
  while ( $blogposts->have_posts() ) :
    $blogposts->the_post();
    $post_id = get_the_ID();
    $permalink = get_permalink($post_id);
    $title = get_field('blog_title',$post_id);
    $date = get_the_date('', $post_id);
    $subAuthorID = $post->post_author;
    $subAuthor = get_user_by('ID',$subAuthorID);
    $subAuthorName = $subAuthor->data->user_nicename;
?>
        <div class="col-md-6 col-lg-4" data-blog="<?php echo $post_id; ?>">
	    	<div class="outer">
				<figure class="inner" style="background-image:url('<?php the_field('thumbnail',$post_id); ?>');">
					<?php
          // echo "<img alt=\"$title\" src=\"".get_field('thumbnail',$post_id)."\">";
          ?>
					<a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" aria-label="Open this post"><span>open this post</span></a>
				</figure>
				<div class="copybox">
          <p>posted by: <?php echo $subAuthorName; ?> on <?php echo $date; ?></p>
					<h2><a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" aria-label="Open this post"><?php echo $title; ?></a></h2>
					<p><?php the_field('blog_synopsis',$post_id); ?></p>
					<a href="<?php echo $permalink; ?>" class="findoutmore">Read more</a>
				</div>
			</div>
        </div>

<?php
  endwhile;
?>
      </div>
<?php
	get_template_part( 'blocks/post-paging','', ['maxpages' => $maxPages,'paged' => $paged] );

	// Clean up after the query and pagination.
	wp_reset_postdata(); 

else :
	echo "<p>boo hoo no posts</p>";
endif;
?>

    </section>
