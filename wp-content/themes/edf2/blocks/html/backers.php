    <section class="module dividers backers">
      <header>
        <h2>Our backers</h2>
        <p>As a non-profit, our only financial focus is maximizing the sale of carbon credits to fund forest protection programmes. Fortunately, until we reach a sufficient volume of sales to cover our operating costs, we are subsidised by the generous backers&nbsp;below.</p>
      </header>
      <div class="row">

        <div class="col-sm-4">
          <div class="copybox">
            <img alt="Rockefeller Foundation" src="<?php echo get_template_directory_uri(); ?>/images/about/partners/rockefeller-foundation.png">
            <p>By identifying and accelerating breakthrough solutions, ideas and conversations, The Rockefeller Foundation works to support climate change solutions globally.</p>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="copybox">
            <img alt="NICFI" src="<?php echo get_template_directory_uri(); ?>/images/about/partners/nicfi.png">
            <p>Norway has pledged up to three billion NOK a year to help save the world’s tropical forests.</p>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="copybox">
            <img alt="EDF" src="<?php echo get_template_directory_uri(); ?>/images/about/partners/edf.png">
            <p>The Environmental Defence Fund is committed to creating lasting solutions to our biggest environmental challenges.</p>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="copybox">
            <img alt="Oak Hill Capital" src="<?php echo get_template_directory_uri(); ?>/images/about/partners/oak-hill.png">
            <p>Oak Hill Capital values our tropical forests’ critical role in protecting biodiversity and supporting sustainable local development.</p>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="copybox">
            <img alt="Good Energies" src="<?php echo get_template_directory_uri(); ?>/images/about/partners/good-energies.png">
            <p>Funding forest protection is one of the ways the Good Energies Foundation works towards their goal of preventing climate change and mitigating its harm.</p>
          </div>
        </div>

      </div>
    </section>
