<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script type="text/javascript">
    var _iub = _iub || [];
    _iub.csConfiguration = {"enableCcpa":true,"countryDetection":true,"consentOnContinuedBrowsing":false,"perPurposeConsent":true,"ccpaAcknowledgeOnDisplay":true,"whitelabel":false,"lang":"en","siteId":2150106,"cookiePolicyId":26866108, "banner":{ "acceptButtonDisplay":true,"customizeButtonDisplay":true,"rejectButtonDisplay":true,"position":"float-top-center" }};
    </script>
    <script type="text/javascript" src="//cdn.iubenda.com/cs/ccpa/stub.js"></script>
    <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>

  <?php
  /*
  add:
  meta tags
  SEO
  facebook OG meta tags
  */

  $isHomepage = ( get_field('homepage') === 'yes' );
  $meta = [
    'page_banner_class' => get_field('page_banner_class'),
    'page_banner_title' => get_field('page_banner_title'),
    'page_banner_copy' => get_field('page_banner_subtitlecopy')
  ];
  $image = get_field('page_banner_image');
  $size = 'full'; // (thumbnail, medium, large, full or custom size)


  wp_head();

  // set &nbsp between last two words in paragraph
  $pageBanner = word_wrapper($meta['page_banner_title']);
  $pageBannerSub = ($meta['page_banner_copy'] !== '' ) ? word_wrapper($meta['page_banner_copy']) :'' ;
  ?>
</head>

<body <?php body_class(); ?> id="content">
<?php wp_body_open(); ?>
    <header id="nav" class="<?php echo $args['page_banner_class']; ?>">

      <nav class="edfnav navbar-dark">
        <a class="home" href="<?php echo site_url(); ?>"><img alt="" src="<?php echo get_template_directory_uri(); ?>/svg/emergent/master-negative.svg"></a>

        <button class="mobilenav" type="button" data-target="#edfnavmenu" aria-controls="edfnavmenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="menu-toggle"><span></span></span>
        </button>

        <div class="menu" id="edfnavmenu">
          <div class="s1">
            <div class="s2">

              <?php wp_nav_menu('main navigation'); ?>
              <div class="secondary">
                
                  <form role="search" method="get" id="searchform" class="searchform form-inline searchForm" action="<?php echo site_url(); ?>">
                  <a href="https://twitter.com/EmergentClimate" target="_blanK" class="twitter" title="twitter"><img alt="twitter" src="<?php echo get_template_directory_uri(); ?>/svg/icons/twitter-white.svg"></a>
                  <a href="https://www.linkedin.com/company/emergent-forest-finance-accelerator/" target="_blank" class="linkedin" title="linkedin"><img alt="twitter" src="<?php echo get_template_directory_uri(); ?>/svg/icons/linkedin-white.svg"></a>
                  <a href="https://www.youtube.com/channel/UC1-0bNIvZ4CwPLc9VWcGCIQ" target="_blank" class="youtube" title="youtube"><img alt="youtube" src="<?php echo get_template_directory_uri(); ?>/svg/icons/youtube-white.svg"></a>
                  <a href="https://www.instagram.com/emergentclimate/" target="_blanK" class="instagram" title="instagram"><img alt="instagram" src="<?php echo get_template_directory_uri(); ?>/svg/icons/Instagram-white.svg"></a>
                  <a href="https://www.facebook.com/EmergentClimate/" target="_blank" class="facebook" title="facebook"><img alt="facebook" src="<?php echo get_template_directory_uri(); ?>/svg/icons/facebook-white.svg"></a>
                  <div class="clipper-out">
                    <div class="clipper-in">
                      <div class="fieldbox">
                        <button class="btn fa ellipsis" id="closeSearchBox" aria-label="close search box"><img alt="Close Search Box" src="<?php echo get_template_directory_uri(); ?>/images/icons/ellipsis.png"></button>
                        <label for="s">Searck for:</label>
                        <input class="form-control" type="search" placeholder="Search" aria-label="Search" aria-expanded="false" id="s" name="s">
                      </div>
                    </div>
                  </div>
                  <button class="btn" aria-controls="search" aria-label="Open search box" id="openSearchBox"><img alt="Open search box" src="<?php echo get_template_directory_uri(); ?>/svg/icons/search.svg"></button>
                  <button class="btn2" id="searchSubmit" type="submit" ><img alt="" src="<?php echo get_template_directory_uri(); ?>/svg/icons/search.svg"></button>
                </form>

              </div>
            </div>
          </div>
        </div>
      </nav>


      <?php
      if (get_field('homepage') == 'yes' ) :
        // home page carousel
        // START
      ?>
<div class="inca" id="incaBanner" data-speed="5000" data-direction="right" data-animation="slide" data-mode="auto" data-has-controls="no" data-has-index="no" data-slides=".item">
  <div class="items">

    <div class="item" id="hb1">
      <div class="headerBanner"><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/02/lp-header-1.jpg"></div>
      <div class="headerText">
        <h1>A breakthrough in forest protection</h1>
        <p>The highest integrity emissions reduction. At an unprecedented scale.</p>
      </div>
    </div>

    <div class="item" id="hb2">
      <div class="headerBanner"><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/03/carousel-2.jpg"></div>
      <div class="headerText">
        <h2>Providing access to the highest integrity credits</h2>
      </div>
    </div>

    <div class="item" id="hb3">
      <div class="headerBanner"><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/03/carousel-3.jpg"></div>
      <div class="headerText">
        <h2>Preserving earth’s biodiversity</h2>
      </div>
    </div>

    <div class="item" id="hb4">
      <div class="headerBanner"><img alt="" src="<?php echo site_url(); ?>/wp-content/uploads/2021/03/carousel-4.jpg"></div>
      <div class="headerText">
        <h2>Supporting global local communities</h2>
      </div>
    </div>

  </div>
</div>

      <?php
      // END
      endif;
      ?>


      <?php
      if (get_field('homepage') !== 'yes' ) :
        // default single image page header
        // START
      ?>
      <div class="headerBanner">
      <?php
        if( $image ) {
          echo wp_get_attachment_image( $image['id'], $size );
        } else {
          // 404 page header id hardcoded - not nice - need to find a better way
          echo wp_get_attachment_image( 83, $size );
        }

      ?>
      </div>

      <div class="headerText">
        <h1><?php echo $pageBanner; ?></h1>
        <?php if ($pageBannerSub !== '') { ?><p><?php echo $pageBannerSub; ?></p><?php } ?>
      </div>

      <?php
      // END
      endif;
      ?>
    </header>
<?php
  /* get_search_form(); */
  /*
  while ( have_posts() ) : the_post();
  endwhile;
  */
?>