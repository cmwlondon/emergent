<?php
/*
Template Name: for-buyers
*/

if ( have_posts() ) {
  the_post();
  $page_id = $post->ID;

  $pageBannerMeta = [
    'page_banner_class' => get_field('page_banner_class'),
    'page_banner_title' => get_field('page_banner_title'),
    'page_banner_copy' => get_field('page_banner_subtitlecopy')
  ];
  get_header( null, $pageBannerMeta );

  // two column copy repeater
  // get_template_part( 'blocks/copy-repeater','',[]);
?>

<section class="module copy fe2" >
<div class="row">
<div class="col-md-12">
<div class="contentBox copyBox">
<div>
<div class="copy">
<p>Emergent helps companies and governments navigate the complexities of evolving standards as well as market and policy uncertainties.</p>
<p>Aligned with the Paris Agreement and other international best practices, Emergent provides a reputable and trusted means for companies to access credits from high-quality forest protection programs, at scale.</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="module copy fe3 dividers" >
<div class="row">
<div class="col-md-6">
<div class="contentBox copyBox">
<div>
<h3>For Climate, CSR, and SDGs Goals –</h3>
<div class="copy">
<p>High-integrity jurisdictional REDD+ credits can help achieve your business’ net zero and climate neutral targets.</p>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="contentBox copyBox">
<div>
<h3>For Paris Agreement Pledges –</h3>
<div class="copy">
<p>Purchase highly credible international mitigation outcomes to help meet Nationally Determined Contributions (NDCs).</p>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="module choices3">
<header>
<h2>Why Choose Emergent?</h2>
</header>
<div class="row">
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>High Integrity</h3>
<div><p>Our market infrastructure unlocks stable sources of sizable, high integrity, jurisdictional supply and streamlines large scale REDD+ credit sales.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box1-1.jpg');"></div>
</div>    
</div>
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>Safeguards</h3>
<div><p>Our high standards of environmental and social performance for contracted jurisdictional REDD+ program credits are verified through the Architecture for REDD+ Transactions (ART), confirming authorizations, regulations, and quality compliance policies in one place.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box2.jpg');"></div>
</div>    
</div>
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>High Volume Transaction Capabilities</h3>
<div><p>Our market infrastructure unlocks stable sources of sizable, high integrity, jurisdictional supply and streamlines large scale REDD+ credit sales.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box3.jpg');"></div>
</div>    
</div>
</div>

<div class="row">
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>Co-benefits</h3>
<div><p>Our REDD+ credits protect natural capital, contributing to climate change mitigation, conservation of biodiversity as well as sustainable development to improve human lives and protect the environment in ways that positively impact Sustainable Development Goals 1, 2, 3, 6, 7, 13, 14 and 15.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box3.jpg');"></div>
</div>    
</div>
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>Flexible Contracting</h3>
<div><p>Our smart purchasing options allow buyers to manage future carbon price exposure through forward purchase agreements – buying credits for a defined period of time in the future.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box2.jpg');"></div>
</div>    
</div>
<div class="col-lg-4">
<div class="boxo">
<div class="boxi">
<div class="relflex">
<div class="copy">
<h3>Philanthropic Funding</h3>
<div><p>Financial contributions and contingency commitments from pay-for-performance models will mobilize additional REDD+ investments for accelerated climate and forest protection.</p></div>
</div>
</div>
</div>
<div class="boxii" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2021/03/box1-1.jpg');"></div>
</div>    
</div>
</div>
</section>
<?php
  // tail
  /*
  page_tail_background
  page_tail_image
  page_tail_quote
  page_tail_speaker
  */
  $pageTailMeta = [
    'background_color_class' => get_field('page_tail_background'),
    'image' => get_field('page_tail_image'),
    'quote1' => get_field('page_tail_quote'),
    'quote2' => get_field('page_tail_speaker')
  ];
  get_template_part( 'blocks/tail','', $pageTailMeta );

  get_footer();
}
?>
