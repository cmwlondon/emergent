    <section class="module exploreLinks">
      <div class="row">
        <div class="col-sm-4">
          <div class="inner">
            <div class="frame"><img alt="About us" src="<?php echo get_template_directory_uri(); ?>/images/home/explore/one.jpg"></div>
            <div class="label">
              <h2>About us <span>Explore</span></h2>
              
            </div>
            <a class="action" title="About us" href="/about-us"></a>
          </div>
        </div>          
        <div class="col-sm-4">
          <div class="inner">
            <div class="frame"><img alt="About us" src="<?php echo get_template_directory_uri(); ?>/images/home/explore/two.jpg"></div>
            <div class="label">
              <h2>How it works <span>Explore</span></h2>
            </div>
            <a class="action" title="How it works" href="/how-it-works"></a>
          </div>
        </div>          
        <div class="col-sm-4">
          <div class="inner">
            <div class="frame"><img alt="About us" src="<?php echo get_template_directory_uri(); ?>/images/home/explore/three.jpg"></div>
            <div class="label">
              <h2>Contact us <span>Explore</span></h2>
            </div>
            <a class="action" title="Contact us" href="/contact-us"></a>
          </div>
        </div>          
      </div>
    </section>
