<?php
// 404 error page
?>

<?php get_header( null, [
  'page_banner_class' => '',
  'page_banner_title' => '404 Page not found',
  'page_banner_copy' => 'Try our <a href="'.site_url().'">homepage</a> instead.',
  'page_banner_image' => get_template_directory_uri().'/images/header/404-header.jpg'
] );

// page content
/**/
/**/

get_template_part( 'blocks/tail','',[
  'background_color_class' => '',
  'image' => '',
  'quote1' => '',
  'quote2' => ''
] );

get_footer();
?>
