/*
Emergent HTML template
December 2020
wordpress
  Advanced Custom Fields Pro
bootstrap

/public/js/common.min.js
/[edf2]/source/js/common.js -> [edf2]/js/theme.min.js
*/

// ----------------------------------------------------------

function VideoPlayer(parameters) {
  this.parameters = parameters;

  this.init();
}

VideoPlayer.prototype = {
  "constructor" : VideoPlayer,
  "template" : function () {var that = this; },
  
  "init" : function () {
    let that = this;

    if ( jQuery('.videoPlayer').length > 0 ) {

      this.videoNode = null;
      this.currentRunningVid = {
        "id": "0",
        "type" : "0",
        "src" : "0"
      };

      // bind behaviour to video 'play' overlays
      jQuery('.videoPlayer a.play').on('click',function(e){
        e.preventDefault();
        that.videoAction(jQuery(this));
      });
    }

  },

  "videoAction" : function(videoTrigger) {
      let vp = videoTrigger.parents('.videoPlayer');

      if(this.currentRunningVid.id !== "0"){
        // stop running video
        switch( this.currentRunningVid.type ) {

          case 'youtube' : {
            this.stopYoutube();
          } break;

          case 'vimeo' : {
            this.stopVimeo();
          } break;

          case 'ted' : {
            this.stopTED();
          } break;

          case 'inline' : {
            this.stopInline();
          } break;
        }
      } else {
        console.log('no video running;');
      }

      // close any open panels
      jQuery('.videoPlayer.open').removeClass('open');

      // collect information on selected video element
      let vID = vp.attr('id');
      let vType = vp.attr('data-type');
      let vSource = vp.attr('data-source');

      console.log("vType: '%s'", vType);

      this.currentRunningVid = {
        "id" : vID,
        "type" : vType,
        "src" : vSource
      };

      // autostart videos
      switch( vType ) {
          case 'youtube' : {
            this.startYoutube(vp);
          } break;

          case 'vimeo' : {
            this.startVimeo(vp);
          } break;

          case 'ted' : {
            this.startTED(vp);
          } break;

          case 'inline' : {
            this.startInline(vp);
          } break;
      }
      // open selected panel
      vp.addClass('open');
  },

  "startYoutube" : function(target) {
      let ytIframe = target.find('iframe').eq(0).get(0);
      ytIframe.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
      this.videoNode = null;  
  },

  "startVimeo" : function(target) {
      console.log('autostart vimeo');
      this.videoNode = null;
  },

  "startTED" : function(target) {
      console.log('autostart TED');
      this.videoNode = null;
  },

  "startInline" : function(target) {
      // start video if it has already been inserted into the page
      let tvt = target.find('video').eq(0);
      tvt.get(0).play();
      this.videoNode = tvt;
  },

  "stopYoutube" : function() {
    let ytIframe = jQuery('#' + this.currentRunningVid.id + ' iframe').get(0);
    ytIframe.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
  },

  "stopVimeo" : function() {
    console.log("stop vimeo video:'%s", this.currentRunningVid.src);
  },


  "stopTED" : function() {
    console.log("stop TED video:'%s", this.currentRunningVid.src);
  },

  "stopInline" : function() {
    this.videoNode.get(0).pause();
    this.videoNode.get(0).currentTime = 0;
  }

};

// ----------------------------------------------------------

function Inca(parameters) {
  this.parameters = parameters;

  this.carousel = parameters.instance;
  this.init();
}

Inca.prototype = {
  "constructor" : Inca,
  "template" : function () {var that = this; },
  
  "init" : function () {
    let that = this;

    // is there a carousel element in the current page?
    if ( jQuery('.inca').length > 0 ) {
      console.log("Inca.init");

      this.installed = false;
      this.animation_is_running = false;
      this.currentItem = 0;

      this.animationType = 'jquery'; // 'css' or 'jquery'

      // this.carousel = jQuery('.inca');

      this.speed = this.carousel.attr('data-speed');
      this.direction = this.carousel.attr('data-direction');
      this.auto = (this.direction === 'left' ) ? 'next' : 'previous' ;
      this.animation = this.carousel.attr('data-animation');
      this.mode = this.carousel.attr('data-mode');
      this.AutoEnabled = ( this.mode ==='auto' );
      this.hasControls = ( this.carousel.attr('data-has-controls') === 'yes' );
      this.hasIndex = ( this.carousel.attr('data-has-index') === 'yes' );
      // this.items = this.carousel.find( this.carousel.attr('data-slides') );
      this.items = this.carousel.find( '.item' );
      this.ItemCount = this.items.length;

      if ( this.hasControls ) {
        this.controls = this.carousel.find('.control');
      }

      if ( this.hasIndex ) {
        this.indicators = this.carousel.find('.index ul li');
      }

      if ( !this.installed) {
        this.install();
      }
    }
  },

  "install" : function () {
    let that = this;

    if (!this.installed) {

      this.installed = false;
        
      // hide all but first slide
      this.items.not(this.items.eq(0)).css({"left" : "0", "opacity":"0"});
      this.items.eq(0).addClass('focus');
      /*
      switch(this.auto){
        case 'next' : {
          this.items.css({"left":"100%"});
        } break;
        case 'previous' : {
          this.items.css({"left":"-100%"});
        } break;
      }
      */
      // this.items.css({"left":"100%"});

      // attach transitionend events to each item
      this.items.each(function(index){
        let titem = document.querySelector('#' + jQuery(this).attr('id'));
        titem.addEventListener('transitionend', () => {
          that.afterTransition(index);
        });
      });

      // position first slide in view
      this.index = 0;
      // this.items.eq(this.index).css({"left":"0%","opacity":"1"});
      this.items.eq(this.index).css({"left":"0%"});
      this.installed = true;

      // this.module.animate({"opacity" : 1},100,function(){});

      if( this.hasIndex ) {
        // bind events to indicators
        this.indicators.on('click',function(){
          if( !that.animation_is_running ) {
            that.jumpto( that.indicators.index(jQuery(this)) );
          }
        });
      }

      if( this.hasControls ) {
        // bind events to previous/next controls
        this.controls.on('click',function(){
          if( !that.animation_is_running ) {
            that.docontrol(jQuery(this).attr('data-action'));
          }
        });
      }

      if( this.AutoEnabled ) {
        this.start(); 
      }
    }
  },

  "start" : function () {
    let that = this;
    
    this.slideAnimation = window.setInterval(() => that.transition(this.auto), this.speed);
  },

  "stop" : function() {
    let that = this;

    // stop interval
    clearInterval(this.slideAnimation);
    this.slideAnimation = null;
  },

  "jumpto" : function (itemIndex) {
    let that = this;
    // console.log("Inca.jumpto: %s", itemIndex);
  },

  "docontrol" : function (action) {
    let that = this;
    // console.log("Inca.docontrol action: '%s'", action);

    if( this.AutoEnabled ) {
      // clear interval
      this.stop();
    }

    // do transition
    this.transition(action);

    if( this.AutoEnabled ) {
      // start interval
      this.start();
    }
  },

  "transition" : function (action) {
    let that = this;

    let slide = -1;

    switch(action) {
      case 'next' : {
        slide = this.nextSlide(this.currentItem);
      } break;
      case 'previous' : {
        slide = this.previousSlide(this.currentItem);
      } break;
    }

    this.transitionToSlide(slide,action);
    that.animation_is_running = false;
  },

  "afterTransition" : function (index) {
    let that = this;

    if ( index !== this.currentItem) {
      /*
      switch(this.auto){
        case 'next' : {
        } break;
        case 'previous' : {
        } break;
      }
      */
      let tx = this.items.eq(index);

      if ( tx.hasClass('transition') ) {
        tx
        .removeClass( 'transition' )
        .css({ "opacity" : "0" });
        that.animation_is_running = false;
      } else {

      }

      /*
      if ( tx.hasClass('cleanup')) {
        // console.log("end cleanup: %s", index);
        tx.removeClass('cleanup').css({"opacity" : "1"});
      } else {
        // console.log("start cleanup: %s", index);
        tx.addClass('cleanup').css({"opacity" : "0", "left" : "100%"});
      }
      */
      
    }
  },

  
  "jumpto" : function (slide) {
    // do nothing if the user clicks on the indicator for the current slide
    if( slide !== this.currentItem) {


      if( this.AutoEnabled ) {
        // clear interval
        this.stop();
      }

      // do transition
      this.transitionToSlide(slide,'next');

      if( this.AutoEnabled ) {
        // start interval
        this.start();
      }
    }
  },

  "nextSlide" : function (slide) {
    let nextSlide = slide + 1;

    if ( nextSlide == this.ItemCount) {
      nextSlide = 0;
    }

    return nextSlide;
  },

  "previousSlide" : function (slide) {
    let nextSlide = slide - 1;

    if ( nextSlide == -1) {
      nextSlide = (this.ItemCount - 1);
    }

    return nextSlide;
  },

  "transitionToSlide" : function(slide,action) {
    let that = this;

    let currentSlide = this.items.eq(this.currentItem);
    let nextSlide = this.items.eq(slide);
    this.oldIndex = this.currentItem;
    this.currentItem = slide;

    // disable controls while frame transition is running
    this.animation_is_running = true;

    switch(action){
      case 'next' : {
        this.transitionR_L(currentSlide,nextSlide,action);
      } break;
      case 'previous' : {
        this.transitionL_R(currentSlide,nextSlide,action);
      } break;
    }

    // this.transitionR_L(currentSlide,nextSlide,action);

    if( this.hasIndex ) {
      // update indicators
      this.indicators.eq(this.oldIndex).removeClass('active');
      this.indicators.eq(this.currentItem).addClass('active');
    }
  },

  "transitionR_L" : function(currentSlide,nextSlide,action) {
    let that = this;
    // console.log("Inca.transitionR_L %s", action );
    // transition: |<|*|<| new frame slides in from right, current frame slide out on left

    // preposition 'next' slide to left (-100%) or right (100%) of screen depending on direction of transition

    if ( this.animationType === 'jquery' ) {
      // jquery animation
      currentSlide
      .css({"left" : "0%"})
      .animate({"left" : "-100%"}, 600,function(){
        currentSlide
        .css({"opacity":"0"})
        .removeClass('focus');
      });

      nextSlide
      .css({"left" : "100%","opacity":"1"})
      .animate({"left" : "0%"}, 600,function(){
        that.animation_is_running = false;
        nextSlide.addClass('focus');
      });
    } else {
      // CSS class/transition
      // .active left:0%
      // .old left:-100%
      // .new left:100%

      nextSlide
      .css({"left" : "100%", "opacity" : "1"});

      currentSlide
      .addClass('transition')
      .css({"left" : "-100%"});
      nextSlide
      .addClass('transition')
      .css({"left" : "0%"});
      // that.animation_is_running = false;
    }


  },

  "transitionL_R" : function(currentSlide,nextSlide, action ) {
    let that = this;
    // console.log("Inca.transitionL_R %s", action );
    // transition: |>|*|>| new frame slides in from left, current frame slide out on right

    if ( this.animationType === 'jquery' ) {
      // jquery animation
      currentSlide
      .css({"left" : "0%"})
      .animate({"left" : "100%"}, 600,function(){
        currentSlide
        .css({"opacity":"0"})
        .removeClass('focus');
      });

      nextSlide
      .css({"left" : "-100%","opacity":"1"})
      .animate({"left" : "0%"}, 600,function(){
        that.animation_is_running = false;
        nextSlide.addClass('focus');
      });

    } else {
      // CSS class/transition
      // .active left:0%
      // .old left:100%
      // .new left:-100%

      nextSlide
      .css({"left" : "-100%", "opacity" : "1"});

      currentSlide
      .addClass('transition')
      .css({"left" : "100%"});
      nextSlide
      .addClass('transition')
      .css({"left" : "0%"});
      // that.animation_is_running = false;
    }

  }

}

// ----------------------------------------------------------

let isIE11 = (function () {
      // true on IE11, false on Edge and other IEs/browsers.
      let isIE11 = !!window.MSInputMethodContext && !!document.documentMode,
          ua = window.navigator.userAgent;

      if (ua.indexOf("AppleWebKit") > 0) {
          return false;
      } else if (ua.indexOf("Lunascape") > 0) {
          return false;
      } else if (ua.indexOf("Sleipnir") > 0) {
          return false;
      }

      array = /(msie|rv:?)\s?([\d\.]+)/.exec(ua);
      version = (array) ? array[2] : '';

      // bug in original script, checks for integer 11, not string '11.0'
      return (version === '11.0') ? true : false;
})();

// ----------------------------------------------------------

(function($) {

  if (isIE11) {
    $('body').addClass('ie11fix');
  }

  // mobile menu
  let menuIsOpen = false;

  $('button.mobilenav').on('click',function(e){
    e.preventDefault();

    // console.log('menu toggle');
    menuIsOpen = !menuIsOpen;
    $('nav.edfnav').toggleClass('open');

    let button = $('button.mobilenav');
    let ariaState = button.attr('aria-expanded');

    if ( ariaState === 'false') {
      button.attr({"aria-expanded" : "true" });
    } else {
      button.attr({"aria-expanded" : "false" });
    }
  });

  // header search box animation
  let sfield = $('#searchField');
  let sform = $('form.searchForm');

  $('#openSearchBox').on('click', function(e) {
    if(!sform.hasClass('expanded')) {
      e.preventDefault();
      sform.addClass('expanded');

      sfield.attr({"aria-expanded" : "true" });

      return false;
    }
  });
  
  $('#closeSearchBox').on('click', function(e) {
    e.preventDefault();
    sform.removeClass('expanded')

    sfield.attr({"aria-expanded" : "false" });
    return false;
  });

  // video panels
  let contentvideos = new VideoPlayer({});

  // activate any inca carousel elements in the page
  let InCarouselElements = jQuery('.inca');

  if ( InCarouselElements.length > 0 ) {
    let InCarouselInstances = [];  
    InCarouselElements.each(function(){
      InCarouselInstances.push(new Inca({
        "instance" : jQuery(this)
      }));
    });
  }

  // team photo,bio show
  if ($('.team').length !== 0 ){
    $('.name h3, .copybox figure').on('click', function (e){
      e.preventDefault();

      let box = $(this).parents('.copybox');
      box.toggleClass('open');
    });
  }

  //faq
  if ($('.faqs').length !== 0 ){
    // major group
    $('.faqGroup > header > h3').on('click', function (e){
      e.preventDefault();

      let box = $(this).parents('section');
      box.toggleClass('open');
    });

    // subgroup
    
    $('article.subfaq > header > h4').on('click', function (e){
      e.preventDefault();

      let box = $(this).parents('article.subfaq');
      box.toggleClass('open');
    });
  }

})(jQuery);
