    <section class="module dividers copy width90percent">
      <div class="row">
        <div class="col-sm-6">
          <div class="copybox">
            <h2>What is ART?</h2>
            <p>ART is an independent organisation that creates the rules, under which carbon credits are issued. It was established to support governments in reducing deforestation at national and subnational&nbsp;levels.</p>
          </div>
          <a href="art" class="stickToBottom">Find out more</a>
        </div>
        <div class="col-sm-6">
          <div class="copybox">
            <h2>What is TREES?</h2>
            <p>TREES is the first purely jurisdictional carbon credit standard, created by ART. It ensures that deforestation reductions are more likely to be permanent, UNFCCC-aligned social and environmental safeguards are in place, and that there is no double counting of&nbsp;results.</p>
          </div>
          <a href="/trees" class="stickToBottom">Find out more</a>
        </div>
        <div class="col-sm-6">
          <div class="copybox">
            <h2>What is ART?</h2>
            <p>ART is an independent organisation that creates the rules, under which carbon credits are issued. It was established to support governments in reducing deforestation at national and subnational&nbsp;levels.</p>
          </div>
          <a href="art" class="stickToBottom">Find out more</a>
        </div>
        <div class="col-sm-6">
          <div class="copybox">
            <h2>What is TREES?</h2>
            <p>TREES is the first purely jurisdictional carbon credit standard, created by ART. It ensures that deforestation reductions are more likely to be permanent, UNFCCC-aligned social and environmental safeguards are in place, and that there is no double counting of&nbsp;results.</p>
          </div>
          <a href="/trees" class="stickToBottom">Find out more</a>
        </div>
      </div>
    </section>
