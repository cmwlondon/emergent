<?php
/*
ACF 'Our Supporters'
supporter_class
supporter_title
supporter_introduction
supporter_dividers ['no','yes']
supporters[
  supporter[
    logo
    logo_url
    logo_caption
    copy
  ],
  ...
]
*/

$supporters = get_field('supporters');
?>

    <section class="module backers <?php if ( get_field('supporter_dividers') === 'yes' ) : ?>dividers<?php endif; ?> <?php the_field('supporter_class'); ?>">
      <header>
        <h2><?php the_field('supporter_title'); ?></h2>
        <p><?php the_field('supporter_introduction'); ?></p>
      </header>

      <div class="row">


<?php
foreach( $supporters AS $index => $supporter ) :
  $logo = $supporter['supporter']['logo'];
  $url = $supporter['supporter']['logo_url'];
  $caption = $supporter['supporter']['logo_caption'];
  $copy = $supporter['supporter']['copy'];

  $columnClass = ( get_field('supporter_class') === 'homeBackers') ? 'col' : 'col-sm-4';
?>
        <div class="<?php echo $columnClass; ?>">
          <div class="copybox">
            <a href="<?php echo $url; ?>" target="_blank" title="<?php echo $caption; ?>"><img alt="<?php echo $caption; ?>" src="<?php echo $logo; ?>"></a>
            <?php if ( $copy !== '' ) : ?>
            <p><?php _e($copy); ?></p>
            <?php endif; ?>
          </div>
        </div>
<?php
endforeach;
?>
      </div>
    </section>
