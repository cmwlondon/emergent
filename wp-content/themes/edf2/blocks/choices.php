<?php
/*
  'dividers' => false,
  'intro-title' => 'why choose us?',
  'intro-copy' => '',
  'items' => [
      'icon' => get_template_directory_uri().'/images/home/markers/co2.png',
      'title' => 'Breakthrough <br>scale',
      'copy' => 'We are pioneers in forest protection. Now for the first time, the private sector has reliable access to carbon credits from massive, jurisdictional-level programmes.',
      'link-text' => '',
      'link-url' => ''
  ],
  ...  
*/

/*
intro-title
intro-copy
dividers
columns
  [
    column
      [
        icon
        heading
        copy
        link_text
        link_url
        link_internal
      ]
  ]
*/
?>

    <section class="module choices <?php if (get_field('dividers')) : echo 'dividers'; endif;?> <?php if (get_field('choices_dynamic') === 'yes' ) : echo 'dynamic'; endif;?>">
      <header>
        <h2><?php echo get_field('intro-title'); ?></h2>
        <?php if (get_field('intro-copy') !== '') : ?><p><?php echo get_field('intro-copy'); ?></p><?php endif;?>
      </header>

      <div class="row">
        <?php
        if( have_rows('columns') ):

          while( have_rows('columns') ) :
            the_row();
            $item = get_sub_field('column');
            
        ?>
            <div class="col-md-4">
              <div class="icon"><?php echo wp_get_attachment_image( $item['icon'], 'full' ); ?></div>
              <div class="inner">
                <h3><?php echo $item['heading']; ?></h3>
                <div><?php echo $item['copy']; ?></div>
              </div>
            </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>
    </section>

