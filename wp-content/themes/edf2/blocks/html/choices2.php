    <section class="module choices dividers">
      <header>
        <h2>why choose us?</h2>
      </header>
      <div class="row">
        <div class="col-sm-4">
          <div class="icon carbon-credits"><img alt="carbon credits" src="<?php echo get_template_directory_uri(); ?>/images/home/markers/backing.png"></div>
          <div class="inner">
            <h3>1. We buy</h3>
            <p>We give the private sector reliable access to carbon credits from massive, jurisdictional-level programmes. A scale that has a transformational impact on the&nbsp;climate.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon trees-credits"><img alt="carbon credits" src="<?php echo get_template_directory_uri(); ?>/images/home/markers/backing.png"></div>
          <div class="inner">
            <h3>2. We sell</h3>
            <p>We give the private sector reliable access to carbon credits from massive, jurisdictional-level programmes. A scale that has a transformational impact on the&nbsp;climate.</p>
            <p>We give the private sector reliable access to carbon credits from massive, jurisdictional-level programmes. A scale that has a transformational impact on the&nbsp;climate.</p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="icon future-credits"><img alt="carbon credits" src="<?php echo get_template_directory_uri(); ?>/images/home/markers/backing.png"></div>
          <div class="inner">
            <h3>3. Companies buy</h3>
            <p>We give the private sector reliable access to carbon credits from massive, jurisdictional-level programmes. A scale that has a transformational impact on the&nbsp;climate.</p>
          </div>
        </div>
      </div>
    </section>
