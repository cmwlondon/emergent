    <footer>
      <a href="<?php echo site_url(); ?>" class="home"><img alt="" src="<?php echo get_template_directory_uri(); ?>/svg/emergent/master-negative.svg"></a>
      <ul class="social">
        <li><a href="https://twitter.com/EmergentClimate" target="_blanK" class="twitter" title="twitter"><img alt="twitter" src="<?php echo get_template_directory_uri(); ?>/svg/icons/twitter-white.svg"></a></li>
        <li><a href="https://www.linkedin.com/company/emergent-forest-finance-accelerator/" target="_blank" class="linkedin" title="linkedin"><img alt="twitter" src="<?php echo get_template_directory_uri(); ?>/svg/icons/linkedin-white.svg"></a></li>
        <li><a href="https://www.youtube.com/channel/UC1-0bNIvZ4CwPLc9VWcGCIQ" target="_blank" class="youtube" title="youtube"><img alt="youtube" src="<?php echo get_template_directory_uri(); ?>/svg/icons/youtube-white.svg"></a></li>
        <li><a href="https://www.instagram.com/emergentclimate/" target="_blanK" class="instagram" title="instagram"><img alt="instagram" src="<?php echo get_template_directory_uri(); ?>/svg/icons/Instagram-white.svg"></a></li>
        <li><a href="https://www.facebook.com/EmergentClimate/" target="_blank" class="facebook" title="facebook"><img alt="facebook" src="<?php echo get_template_directory_uri(); ?>/svg/icons/facebook-white.svg"></a></li>
      </ul>
      <ul class="general">
        <li><a href="<?php echo site_url(); ?>/public-disclosure">Public Disclosure</a></li>
        <li><a href="<?php echo site_url(); ?>/privacy-policy">Privacy Policy</a></li>
      </ul>
      <p>&copy;Emergent 2021</p>
    </footer>

	<?php wp_footer(); ?>
  <!--
  <script type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/theme.min.js"></script>
  -->
</body>
</html>