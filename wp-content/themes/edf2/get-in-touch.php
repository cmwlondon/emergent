<?php
/*
Template Name: get-in-touch
*/

$pageBannerMeta = [
  'page_banner_class' => get_field('page_banner_class'),
  'page_banner_title' => get_field('page_banner_title'),
  'page_banner_copy' => get_field('page_banner_subtitlecopy')
];
get_header( null, $pageBannerMeta );

// page content
/**/
?>
    <section class="module hubspot">
      <div class="row">
        <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
          <script>
            hbspt.forms.create({
            portalId: "8299570",
            formId: "e9319aad-2184-4697-9636-93dccce77533"
          });
          </script>
        </div>
      </div>
    </section>
<?php
/**/

// tail
$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
