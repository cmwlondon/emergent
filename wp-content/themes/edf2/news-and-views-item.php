<?php
/*
Template Name: news-and-views-item
*/
?>

<?php get_header(); ?>
<!-- opening body tag in header.php -->

    <section class="module generalContent lightGreenBG ComingSoon">
      <header>
        <h2>Coming Soon</h2>
      </header>
    </section>

    <section class="module tail lightGreenBG">
      <div class="background">
        
        <div class="wave">
          <img class="ablogo" src="<?php echo get_template_directory_uri(); ?>/svg/emergent/green.svg">
          <img class="curve" src="<?php echo get_template_directory_uri(); ?>/svg/wave.svg">
          <div class="message">
            <h2>Get in touch</h2>
            <p>If you have any questions about our high-integrity carbon credits, please don’t hesitate to email us at <a href="mailto:info@emergentclimate.com">info@emergentclimate.com</a>.</p>
          </div>
        </div>

      </div>
    </section>


<!-- closing body tag in footer.php -->
<?php get_footer(); ?>
