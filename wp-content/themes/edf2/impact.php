<?php
/*
Template Name: impact
*/

$pageBannerMeta = [
  'page_banner_class' => get_field('page_banner_class'),
  'page_banner_title' => get_field('page_banner_title'),
  'page_banner_copy' => get_field('page_banner_subtitlecopy')
];
get_header( null, $pageBannerMeta );

// page content
/**/
get_template_part( 'blocks/coming-soon','',[] );
/**/

$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
