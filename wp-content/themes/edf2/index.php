<?php
/* single post page */

/*
blog_title
blog_synopsis
thumbnail
blog_has_a_video
blog_video{
	
}
blog_image

WP_Post['post_author']
*/

if ( have_posts() ) {
  the_post();

$post_id = get_the_ID();
$hasVideo = get_field('blog_has_a_video', $post_id);
$maindate = get_the_date('', $post_id);
$authorID = $post->post_author;
$author = get_user_by('ID',$authorID);
$authorName = $author->data->user_nicename;

$pageBannerMeta = [
  'page_banner_class' => 'post',
  'page_banner_title' => '',
  'page_banner_copy' => '',
  'page_banner_image' => get_field('page_banner_image')
];
get_header( null, $pageBannerMeta );
?>
<section class="module blogpost">

	<div class="row">

    <section class="col-lg-3 recent">
      <header>
        <h2>Recent Posts</h2>
      </header>
      <div class="row">
<?php
  // get 3 most recent posts excluding the post being viewed 
  // most recent first
  $querySettings = [
    'post_type' => ['post'],
    'post_status' => ['publish'],
    'posts_per_page' => 3,
    'post__not_in' => [ $post_id ] // skip over featured post
  ];
  $blogposts = new WP_Query($querySettings);

  // Load posts loop.
  while ( $blogposts->have_posts() ) :
    $blogposts->the_post();
    $subpost_id = get_the_ID();
    $permalink = get_permalink($subpost_id);
    $title = get_field('blog_title',$subpost_id);
    $subDate = get_the_date('', $subpost_id);
    $subAuthorID = $post->post_author;
    $subAuthor = get_user_by('ID',$subAuthorID);
    $subAuthorName = $subAuthor->data->user_nicename;
?>
        <div class="col" data-blog="<?php echo $subpost_id; ?>">
          <div class="outer">
            <figure class="inner" style="background-image:url('<?php the_field('thumbnail',$subpost_id); ?>');">
              <a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" aria-label="Open this post"><img alt="<?php echo $title; ?>" src="<?php the_field('thumbnail',$subpost_id); ?>"></a>
            </figure>
            <div class="copybox">
              <p>Posted by: <span><?php echo $subAuthorName; ?></span> on <span><?php echo $subDate; ?></span></p>
              <h3><a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" aria-label="Open this post"><?php echo $title; ?></a></h3>
            </div>
          </div>
        </div>
<?php
  endwhile;

  wp_reset_postdata(); 
?>
      </div>
    </section>

		<section class="main">
      <header>
        <h1><?php echo get_field('blog_title', $post_id); ?></h1>
        <p><?php echo "posted by $authorName on $maindate"; ?></p>
      </header>
      <div>
<?php
switch( $hasVideo ) :
case 'yes' : {
  $item = get_field('blog_video', $post_id);
?>
        <div class="videoPlayer" data-type="<?php echo $item['blog_video_format']; ?>" data-source="<?php echo $item['blog_video_source']; ?>" id="fwv01">
          <div class="poster"><img alt="" src="<?php echo $item['blog_video_poster']; ?>"></div>
          <div class="video <?php echo $item['blog_video_format']; ?>">

          <?php 
              switch($item['blog_video_format']) {
                case 'youtube' : {
              ?>
              <iframe id="yt01" class="yt_player_iframe" src="https://www.youtube.com/embed/<?php echo $item['blog_video_source']; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="true" allowscriptaccess="always" frameborder="0"></iframe>
              <?php
                } break;
                case 'vimeo' : {
              ?>
              <iframe src="https://player.vimeo.com/video/<?php echo $item['blog_video_source']; ?>?color=62819e&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
              <?php
                } break;
                case 'ted' : {
              ?>
              <?php
                } break;
                case 'inline' : {
              ?>
              <video preload="metadata" controls="controls" playsinline="" noloop="">
                <source type="video/mp4" src="<?php echo $item['blog_video_source']; ?>">
              </video>
              <?php
                } break;
              }
          ?>    

          </div>
          <?php // echo "<h2>".$item['blog_video_caption']."</h2>"; ?>
          <a href="" class="play"></a>
        </div>

<?php
} break;
case 'no' : {
	$blogImageData = get_field('blog_image', $post_id);
	$blogImageURL = $blogImageData['url'];
?>
<figure><img alt="<?php the_field('blog_title', $post_id); ?>" src=" <?php echo $blogImageURL; ?>"></figure>
<?php  
} break;
endswitch;
?>
			<?php the_content(); ?>
    </div>
    <a href="<?php echo site_url(); ?>/news-views">Go back to News and views</a>
		</section>
	</div>
	<div class="row end">
		<div class="col">
			
		</div>
	</div>
</section>
<?php  
}

$pageTailMeta = [
  'background_color_class' => get_field('page_tail_background'),
  'image' => get_field('page_tail_image'),
  'quote1' => get_field('page_tail_quote'),
  'quote2' => get_field('page_tail_speaker')
];
get_template_part( 'blocks/tail','', $pageTailMeta );

get_footer();
?>
