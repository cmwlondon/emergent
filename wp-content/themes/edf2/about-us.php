<?php
/*
Template Name: about-us
*/

if ( have_posts() ) {
  the_post();
  $page_id = $post->ID;

  $pageBannerMeta = [
    'page_banner_class' => get_field('page_banner_class'),
    'page_banner_title' => get_field('page_banner_title'),
    'page_banner_copy' => get_field('page_banner_subtitlecopy')
  ];
  get_header( null, $pageBannerMeta );

  // two column content
  // class: module copy width90percent largemargins
  get_template_part( 'blocks/copy','',[]);

  // choices
  get_template_part( 'blocks/choices3','',[]);

  // backers module
  get_template_part( 'blocks/backers','',[]);

  // team
  // display custom post type 'team_members' appears in dashboard under 'Our team'
  // control order in pages -> 'About us'
  // control individual team member data under 'Our Team'
  get_template_part( 'blocks/team','',[]);

  // board members
  // displays custom post type 'boardmembers' appears in dashboard under 'Our Board Members'
  // control order in pages -> 'About us'
  // control individual team member data under 'Our Board Members'
  get_template_part( 'blocks/board','',[]);

  // tail
  /*
  page_tail_background
  page_tail_image
  page_tail_quote
  page_tail_speaker
  */
  $pageTailMeta = [
    'background_color_class' => get_field('page_tail_background'),
    'image' => get_field('page_tail_image'),
    'quote1' => get_field('page_tail_quote'),
    'quote2' => get_field('page_tail_speaker')
  ];
  get_template_part( 'blocks/tail','', $pageTailMeta );

  get_footer();
}
?>
